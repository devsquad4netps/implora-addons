# -*- coding: utf-8 -*-
{
    'name': "Pay Order Validation",
    'summary': """Pay Order Print Out Validation""",
    'description': """Pay Order Print Out Validation""",
    'author': "Dani R.",
    'website': "http://www.yourcompany.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Report',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base', '4net_reports'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'report/pay_order.xml',
    ],
}
