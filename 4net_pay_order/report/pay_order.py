# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class AccountMove(models.Model):
    _inherit = 'account.move'

    def action_print_pay_order(self):
        return self.env.ref('4net_reports.report_payment_order_inv').report_action(self)

class ReportPaymentReceiptInvoice(models.AbstractModel):
    _inherit = 'report.4net_reports.report_payment_receipt_inv'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids)
        for doc in docs:
            if doc.move_type == 'out_invoice':
                raise ValidationError('Pay Order is not available for Customer Invoice.')
        return super()._get_report_values(docids=docids, data=data)
