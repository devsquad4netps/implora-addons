from odoo import api, fields, models

class approval(models.Model):
    _inherit="approval.request"

    def action_print(self):
        return self.env.ref('4net_approval.act_print_approval_request').report_action(self)