from ast import Store
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class VendorBillInherit(models.Model):
    _inherit ='account.move'

    currency_rate = fields.Float(
        string="Currency Rate",readonly=True
    )
    # 'currency_rate'    : rec.currency_id.rate,

    @api.onchange('currency_id')
    def _onchange_currency_id(self):
        rate_obj = self.env['res.currency.rate']
        for rec in self:
            if rec.currency_id:
                # rate = rate_obj.search([('currency_id', '=', rec.currency_id.id),('company_id', '=', self.env.company.id)])
                rate = self.env['res.currency.rate'].search([('id', '=', rec.currency_id.id)], limit=1).inverse_company_rate
                rec.write({
                    'currency_rate' : rate,
                })


