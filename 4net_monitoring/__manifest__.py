# -*- coding: utf-8 -*-
{
    'name': "Monitoring Report",
    'summary': """
            * Monitoring Account
            * Outstanding Product
        """,
    'description': """
            * Monitoring Account
            * Outstanding Product
        """,
    'author': "Dani R.",
    'website': "http://www.yourcompany.com",
    'category': 'Accounting',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'purchase', 'stock'],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/monitor_account.xml',
        'report/monitor_product.xml',
    ],
}
