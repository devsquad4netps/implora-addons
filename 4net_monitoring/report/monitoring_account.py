# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class PurchaseBillReport(models.Model):
    _name = 'purchase.bill.report'
    _auto = False

    amount = fields.Float(string='Amount', readonly=True)
    product_id = fields.Many2one('product.product', readonly=True)
    receipt_date = fields.Datetime(related='picking_id.date_done')
    partner_id = fields.Many2one('res.partner', string='Vendor Name', readonly=True)
    purchase_id = fields.Many2one('purchase.order', string='Purchase Order', readonly=True)
    account_id = fields.Many2one('account.account', string='Account', compute='get_account_id')
    invoice_status = fields.Selection([('no', 'Nothing to Bill'), ('to invoice', 'Waiting Bills'),
        ('invoiced', 'Fully Billed')], string='Billing Status', readonly=True)
    picking_id = fields.Many2one('stock.picking', string='Receipt Number', compute='get_picking_id')
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'purchase_bill_report')
        self.env.cr.execute(
            """ 
                CREATE OR REPLACE VIEW purchase_bill_report AS 
                (
                    SELECT 
                        pol.id, po.id as purchase_id, po.partner_id as partner_id, 
                        po.amount_untaxed as amount, po.invoice_status, pol.product_id,
                        NULL as account_id, NULL as picking_id, NULL as receipt_date
                    FROM 
                        purchase_order_line as pol
                    JOIN
                        purchase_order as po
                        ON (pol.order_id = po.id)
                    WHERE 
                        po.state in ('purchase', 'done') 
                        AND po.invoice_status in ('to invoice', 'invoiced')
                )
            """
        )

    def get_picking_id(self):
        for record in self:
            domain = [('purchase_id', '=', record.purchase_id.id)]
            # print("### domain ###", domain, record.account_id.user_type_id.name)
            picking_id = self.env['stock.picking'].search(domain, limit=1)
            record.picking_id = picking_id

    def get_account_id(self):
        for record in self:
            accounts = record.product_id.product_tmpl_id.get_product_accounts()
            # print("### accounts ###", accounts)
            record.account_id = accounts['expense']