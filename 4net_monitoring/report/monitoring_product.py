# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools

class PurchaseReceiptReport(models.Model):
    _name = 'purchase.receipt.report'
    _description = 'Purchase Receipt Report'
    _auto = False

    # * purchase
    partner_id = fields.Many2one('res.partner', string='Vendor Name', readonly=True)
    purchase_id = fields.Many2one('purchase.order', string='Purchase Order', readonly=True)
    order_line_id = fields.Many2one('purchase.order.line', string='Order Line', readonly=True)

    # * product
    product_id = fields.Many2one('product.product', readonly=True)
    price_unit = fields.Float(string='Unit Price')
    price_subtotal = fields.Float(string='Subtotal')
    product_uom = fields.Many2one('uom.uom', string='Unit of Measure')
    product_qty = fields.Float()
    qty_received = fields.Float()
    qty_to_receive = fields.Float(string='Qty Outstanding')
    
    # * picking
    picking_id = fields.Many2one('stock.picking', string='Receipt Number')
    receipt_date = fields.Datetime()
    scheduled_date = fields.Datetime()
    picking_state = fields.Selection(string='Status', selection=[('draft', 'Draft'),
        ('waiting', 'Waiting Another Operation'), ('confirmed', 'Waiting'),
        ('assigned', 'Ready'), ('done', 'Done'), ('cancel', 'Cancelled')])
    receipt_state = fields.Selection(string='Receive Status', 
        selection=[('waiting', 'Waiting Receipt'), ('partial', 'Partial Receipt')])

    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'purchase_receipt_report')
        self.env.cr.execute(
            """ 
                CREATE OR REPLACE VIEW purchase_receipt_report AS 
                (
                    SELECT 
                        pol.id, pol.id as order_line_id, po.id as purchase_id, 
                        po.partner_id as partner_id, 
                        pol.product_id, pol.price_unit, 
                        pol.price_subtotal, pol.product_uom, pol.product_qty, pol.qty_received,
                        (pol.product_qty - pol.qty_received) as qty_to_receive,
                        sp.id as picking_id, sp.date_done as receipt_date, 
                        sp.state as picking_state, sp.scheduled_date as scheduled_date, 
                        CASE 
                            WHEN pol.qty_received <= 0 
                                THEN 'waiting'
                            WHEN pol.qty_received > 0 AND pol.qty_received < pol.product_qty 
                                THEN 'partial'
                            ELSE NULL
                        END
                        as receipt_state
                    FROM purchase_order_line pol
                    JOIN purchase_order po ON (pol.order_id = po.id)
                    JOIN stock_move sm ON (pol.id = sm.purchase_line_id)
                    JOIN stock_picking sp ON (sm.picking_id = sp.id)
                    WHERE (pol.product_qty - pol.qty_received) > 0 AND pol.price_unit > 0
                    AND sp.state NOT IN ('cancel', 'done')
                )
            """
        )

class ProductMonitoringReport(models.Model):
    _name = 'product.monitoring.report'
    _description = 'Monitoring Product'
    _order = 'purchase_id asc'

    purchase_receipt_id = fields.Many2one('purchase.receipt.report')
    purchase_id = fields.Many2one('purchase.order', string='Source Document')
    product_id = fields.Many2one('product.product', related='purchase_receipt_id.product_id', store=True)
    qty_to_receive = fields.Float(string='Qty Outstanding', related='purchase_receipt_id.qty_to_receive')
    price_unit = fields.Float(string='Unit Price', related='purchase_receipt_id.price_unit')
    price_subtotal = fields.Float(string='Subtotal', related='purchase_receipt_id.price_subtotal')
    product_uom = fields.Many2one('uom.uom', string='Unit of Measure', related='purchase_receipt_id.product_uom')
    receipt_date = fields.Datetime(related='purchase_receipt_id.receipt_date')
    scheduled_date = fields.Datetime(related='purchase_receipt_id.scheduled_date')
    picking_state = fields.Selection(related='purchase_receipt_id.picking_state')
    # receipt_state = fields.Selection(related='purchase_receipt_id.receipt_state', store=True)
    receipt_state = fields.Selection(string='Receive Status', 
        selection=[('waiting', 'Waiting Receipt'), ('partial', 'Partial Receipt')])

    def get_purchase_receipt(self):
        purchase_receipt_obj = self.env['purchase.receipt.report']
        domain = [('product_id', '!=', False)]
        purchase_receipt_ids = purchase_receipt_obj.search(domain, order='purchase_id desc')
        # print("## purchase_receipt_ids ###", purchase_receipt_ids)
        print("## product in purchase_receipt_ids ###", [x.product_id.id for x in purchase_receipt_ids])
        
        results = []
        order_line_ids = []
        for x in purchase_receipt_ids:
            if x.order_line_id.id not in order_line_ids:
                order_line_ids.append(x.order_line_id.id)
                results.append(x)
        return results
    
    def init(self):
        to_create_ids = self.get_purchase_receipt()
        for tc in to_create_ids:
            if tc.id not in [x.purchase_receipt_id.id for x in self.search([])]:
                self.create({
                        'purchase_receipt_id': tc.id, 
                        'purchase_id': tc.purchase_id.id,
                        'receipt_state': tc.receipt_state,
                    })

    catch_record = fields.Boolean(compute='_compute_record')

    def _compute_record(self):
        self.init()
        for record in self:
            record.catch_record = False
    