odoo.define('4net_account.button_wizard_controller', function(require) {
    'use strict';

    var core = require('web.core');
    var ListController = require('web.ListController');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var _t = core._t;

    ListController.include({
        renderButtons: function ($node) {
            this._super.apply(this, arguments);
            if (this.$buttons) {
                this.$buttons.find('.oe_action_button_wizard').click(this.proxy('action_journal_wizard_date')) ;
            }
        },
        action_journal_wizard_date: function () {

            this.do_action({
                type: "ir.actions.act_window",
                name: "Filter Wizard",
                res_model: "journal.wizard.date",
                views: [[false, 'form']],
                target: 'new',
                view_type: 'form',
                view_mode: 'form',
            });
        },
    });
    
});