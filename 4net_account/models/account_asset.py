from odoo import fields, models, _, api

class AccountAsset(models.Model):
    _inherit = "account.asset"
    asset_id = fields.Integer(string='Asset Id',readonly=True)
    tag_number = fields.Char(string='Tag Number')
    
    @api.model
    def create(self, vals):
        assetid_sequence = self.env['ir.sequence']
        vals['asset_id'] = assetid_sequence.next_by_code('asset.id')
        return super(AccountAsset, self).create(vals)