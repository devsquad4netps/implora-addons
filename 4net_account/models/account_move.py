from odoo import api, fields, models, Command, _
from odoo.exceptions import UserError, ValidationError

class AccountMove(models.Model):
    _inherit = "account.move"
    
    move_type = fields.Selection(selection=[
            ('entry', 'Journal Entry'),
            ('out_invoice', 'Customer Invoice'),
            ('out_refund', 'Customer Credit Note'),
            ('in_invoice', 'Internal Number Bill'),
            ('in_refund', 'Vendor Credit Note'),
            ('out_receipt', 'Sales Receipt'),
            ('in_receipt', 'Purchase Receipt'),
        ], string='Type', required=True, store=True, index=True, readonly=True, tracking=True,
        default="entry", change_default=True)

    invoice_date = fields.Date(
        string='Invoice/Bill Date', default=fields.Date.context_today, readonly=True, index=True, copy=False, states={'draft': [('readonly', False)]})

    def mass_set_to_draft(self):
        for rec in self:
            if rec.state == 'draft':
                raise UserError(_(f'Doc {rec.name} Masih Status Draft'))
            if rec.invoice_payments_widget != 'false':
                raise UserError(_(f'Doc {rec.name} Sudah di lakukan pembayaran'))
            rec.button_draft()
    
    def action_journal_wizard_date(self):
        data_view = self.env.ref('4net_account.view_journal_wizard_date').id
        return data_view


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    def _get_computed_name(self):
        res = super()._get_computed_name()
        if self.product_id:
            if self.move_id.move_type in ['in_invoice','in_receipt']:
                res = self.product_id.description_purchase
            if self.move_id.move_type in ['out_invoice','out_refund']:
                res = self.product_id.description_sale
        return res