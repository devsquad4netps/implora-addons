from odoo import _, fields, models
from odoo.osv import expression
import datetime

class JournalWizardDate(models.TransientModel):
    _name = 'journal.wizard.date'
    _description = 'Journal Wizard Date'

    date_start = fields.Date("Date From", required=True)
    date_end = fields.Date("Date To", required=True)


    def open_at_date(self):
        action = self.env.ref('account.action_move_journal_line').read()[0]  
        action['domain'] = [('date','<=',self.date_end),('date','>=',self.date_start)]
        date_start = datetime.datetime.strptime(str(self.date_start), '%Y-%m-%d').strftime('%d %B %Y')
        date_end = datetime.datetime.strptime(str(self.date_end), '%Y-%m-%d').strftime('%d %B %Y')
        action['display_name'] = f'{date_start} - {date_end}'
        return action