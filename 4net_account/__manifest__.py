# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'              : 'Custom Account',
    'version'           : '1.2',
    'category'          : 'Accounting',
    'sequence'          : 35,
    'summary'           : 'Purchase Order, Payment Receipt, Invoice Report',
    'description'       : "",
    'website'           : '',
    'depends'           : ['account','purchase','web','account_asset'],
    'data'              : [
                            'security/ir.model.access.csv',
                            'data/ir_sequence_data.xml',
                            'security/payment_security.xml',
                            'security/account_security.xml',
                            'wizard/journal_wizard_date_view.xml',
                            'views/account_asset_view.xml',
                            'views/account_payment_view.xml',
                            'views/account_move_views.xml',
                            'views/inherit_vendor_bill.xml',
                        ],
    'assets'            : {
        'web.assets_backend': [
                                '4net_account/static/src/js/button_wizard_controller.js',
                            ],
        'web.assets_qweb'   : [
                                '4net_account/static/src/xml/button_wizard_date.xml',
                            ],
    },
    'installable'       : True,
    'auto_install'      : False,
    'application'       : True,
    'license'           : 'LGPL-3',
}
