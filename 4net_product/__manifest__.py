
{
    'name'              : 'Custom Products & Pricelists',
    'version'           : '15.0.0.1',
    'category'          : 'Sales/Sales',
    # 'depends'           : ['base', 'product'],
    'depends'           : ['base', 'product', 'purchase'],
    'description'       : """ """,
    # 'data'              : [],
    'data'              : [
        'views/product.xml',
        'views/purchase.xml',
        'views/invoice.xml',
    ],
    'installable': True,
    'auto_install': False,
}
