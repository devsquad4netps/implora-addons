# -*- coding: utf-8 -*-
{
    'name'          : 'Multiple Discounts on Invoice',
    'author'        : 'Tian',
    'category'      : 'Invoices & Payments',
    'summary'       : 'Multiple Discounts in Invoice',
    'description'   : """Multiple Discounts inInvoice""",
    'website'       : '',
    'version'       : '1.0',
    'sequence'      : 1,
    'depends'       : ['account', 'base', 'account_accountant'],
    'data':          [
                        # 'security/ir.model.access.csv',
                        'views/account_tax_views.xml',
                        'views/account_account_view.xml',
                        'views/res_config_view.xml',
                        'views/account_invoice_view.xml',
                      ],
    'assets'        : {
                        'web.assets_qweb': [
                            '4net_multi_discounts/static/src/xml/tax_totals.xml',
                        ],
                     },
    'installable': True,
}