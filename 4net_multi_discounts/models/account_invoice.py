from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp
from odoo.tools.misc import formatLang, format_date, get_lang
from collections import defaultdict
from odoo.tools import float_round

move_type_discount = ['out_invoice','out_refund']
ks_name = "Multi Discount"

class AccountMove(models.Model):
    _inherit = "account.move"

    discount_type = fields.Selection([
        ('percent', 'Percentage'), ('amount', 'Amount')], string='Discount Type', required=True,
        readonly=True, states={'draft': [('readonly', False)]}, default='amount'
    )
    discount_rate = fields.Float(
        'Discount Rate', readonly=True, states={'draft': [('readonly', False)]}
    )
    account_discount_id = fields.Many2one(
        'account.account', 'Account Discount', related='company_id.account_discount_id'
    )
    amount_discount_line =fields.Float(
        'Discount Line', compute="_compute_multi_discount", digits='Product Price'
    )
    amount_discount_global = fields.Float(
        'Discount Global', compute="_compute_multi_discount", digits='Product Price'
    )
    rounding_discount_amount = fields.Float(
        'Rounding Discount', digits='Product Price'
    )
    total_discount_amount = fields.Float(
        'Total Discount', compute="_compute_multi_discount", digits='Product Price'
    )
    account_tax_inv_id = fields.Many2one(
        'account.account', 'Account Global Tax', compute="_compute_multi_discount"
    )
    amount_tax_inv = fields.Float(
        'Amount Tax', compute="_compute_multi_discount", digits='Product Price'
    )
    amount_total_inv = fields.Float(
        "Amount Total Inv", digits='Product Price'
    )
    type_update = fields.Selection(
        string='Type Update', 
        selection=[('discount','Discount'), ('product','Product')]
    )
    new_create = fields.Boolean(
        "New Data", default=True
    )

    
    @api.constrains('discount_rate')
    def ks_check_discount_value(self):
        for rec in self:
            if rec.discount_type == "percent":
                if rec.discount_rate > 100 or rec.discount_rate < 0:
                    raise ValidationError('You cannot enter percentage value greater than 100.')
        # else:
        #     if self.discount_rate < 0 or self.amount_untaxed < 0:
        #         raise ValidationError(
        #             'You cannot enter discount amount greater than actual cost or value lower than 0.')
                
    def _compute_tax_global(self):
        for rec in self:
            tax_ids = False
            inv_line = rec.invoice_line_ids.filtered(lambda l: l.tax_ids)
            if inv_line: 
                tax_ids = inv_line[0].tax_ids
            return tax_ids
    
    def update_discount_global(self, amount):
        for rec in self:
            amount_discount = 0
            if rec.discount_type == 'percent':
                if rec.discount_rate > 0 and amount > 0:
                    amount_discount = amount * (float(rec.discount_rate) / 100.0)
            else:
                amount_discount = rec.discount_rate
            return amount_discount
    
    def tax_include(self, tax, amount_untaxed):
        for rec in self:
            subtotal = amount_untaxed - rec.amount_discount_global
            dpp = subtotal / 1.11
            amount_tax = 0
            if tax:
                taxes_res = tax.compute_all(
                    dpp,
                    quantity=1.0,
                    currency=rec.currency_id,
                    handle_price_include=False
                )
                amount_tax = (dpp * 1.11) - dpp
                account_tax_inv_id = taxes_res['taxes'][0]['account_id']
            rec.amount_total_inv = dpp + amount_tax
            return amount_tax, account_tax_inv_id
    
    def tax_exclude(self, tax, amount_untaxed):
        for rec in self:
            subtotal = amount_untaxed - rec.amount_discount_global
            amount_tax = 0
            if tax:
                taxes_res = tax.compute_all(
                    subtotal,
                    quantity=1.0,
                    currency=rec.currency_id,
                )
                amount_tax = rec.amount_tax_signed = taxes_res['taxes'][0]['amount']
                account_tax_inv_id = taxes_res['taxes'][0]['account_id']
            rec.amount_total_inv = subtotal + amount_tax
            return amount_tax, account_tax_inv_id
                                
    @api.depends("rounding_discount_amount","discount_rate","discount_type","invoice_line_ids","invoice_line_ids.multi_discount","invoice_line_ids.tax_ids")
    def _compute_multi_discount(self):
        for rec in self:
            rec.amount_discount_line = rec.amount_discount_global = rec.total_discount_amount = rec.amount_tax_inv = 0
            rec.account_tax_inv_id = False
            if rec.move_type in move_type_discount:
                amount_untaxed = sum(rec.invoice_line_ids.mapped("price_subtotal_inv"))
                rec.amount_untaxed = amount_untaxed
                rec.amount_discount_line = sum(rec.invoice_line_ids.mapped("amount_discount"))
                rec.amount_discount_global = rec.update_discount_global(amount_untaxed)
                currencies = rec._get_lines_onchange_currency().currency_id
                currency = currencies if len(currencies) == 1 else rec.company_id.currency_id
                total_discount = rec.amount_discount_line + rec.amount_discount_global
                tax = rec._compute_tax_global()
                amount_tax = 0
                account_tax_inv_id = False
                if not tax:
                    rec.amount_total_inv = amount_untaxed - rec.amount_discount_global
                else:
                    if tax.type_invoice == 'include':
                        amount_tax, account_tax_inv_id = rec.tax_include(tax, amount_untaxed)
                        total_discount = total_discount / 1.11
                    else:
                        amount_tax, account_tax_inv_id = rec.tax_exclude(tax, amount_untaxed)
                rec.amount_tax_inv = amount_tax
                rec.account_tax_inv_id = account_tax_inv_id
                disocunt = float_round(total_discount + rec.rounding_discount_amount, precision_digits=4, rounding_method='DOWN')
                rec.total_discount_amount = round(disocunt, currency.decimal_places)
    
    @api.onchange('discount_rate', 'discount_type', 'line_ids','total_discount_amount')
    def _recompute_universal_discount_lines(self):
        for rec in self:
            type_list = ['out_invoice', 'out_refund']
            if rec.move_type in type_list:
                if rec.is_invoice(include_receipts=True):
                    currencies = rec._get_lines_onchange_currency().currency_id
                    currency = currencies if len(currencies) == 1 else rec.company_id.currency_id
                    amount_discount = round(rec.total_discount_amount, currency.decimal_places)
                    amount = amount_discount if rec.move_type == 'out_invoice' else -amount_discount
                    in_draft_mode = self != self._origin
                    terms_lines = self.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable'))
                    already_exists = self.line_ids.filtered(lambda line: line.account_id.id == rec.account_discount_id.id)
                    line_ids_tax = rec.line_ids.filtered(lambda l : l.account_id.id == rec.account_tax_inv_id.id)
                    account_partner_id = rec.partner_id.property_account_receivable_id
                    line_ids_partner = rec.line_ids.filtered(lambda l : l.account_id.id == account_partner_id.id)
                    amount_tax = -rec.amount_tax_inv if rec.move_type == 'out_invoice' else rec.amount_tax_inv
                    amount_tax_inv = round(amount_tax, currency.decimal_places)
                    amount_total = rec.amount_total if rec.move_type == 'out_invoice' else -rec.amount_total
                    if line_ids_partner:
                        line_ids_partner.update({
                            'debit': abs(amount_total) if amount_total > 0.0 else 0.0,
                            'credit': abs(amount_total) if amount_total < 0.0 else 0.0,
                            'amount_currency': amount_total,
                        })
                    if line_ids_tax:
                        line_ids_tax.update({
                            'debit': abs(amount_tax_inv) if amount_tax_inv > 0.0 else 0.0,
                            'credit': abs(amount_tax_inv) if amount_tax_inv < 0.0 else 0.0,
                            'amount_currency': amount_tax_inv,
                        })

                    if already_exists:
                        if amount != 0:
                            already_exists.update({
                                'name': ks_name,
                                'debit':  abs(amount) if amount > 0.0 else 0.0,
                                'credit': abs(amount) if amount < 0.0 else 0.0,
                                'amount_currency': amount,
                            })
                            balance = round(sum(rec.line_ids.mapped('debit')) - sum(rec.line_ids.mapped('credit')), currency.decimal_places)
                            if balance != 0 and balance > -0.1 and balance < 0.1:
                                discount_rounding = -balance if balance > 0 else abs(balance)
                                if rec.move_type == 'out_refund':
                                    discount_rounding = abs(discount_rounding) if discount_rounding < 0.0 else discount_rounding
                                rec.rounding_discount_amount = discount_rounding
                        else:
                            duplicate_id = self.line_ids.filtered(lambda line: line.account_id.id == rec.account_discount_id.id)
                            self.line_ids = self.line_ids - duplicate_id
                    else:
                        new_tax_line = self.env['account.move.line']
                        create_method = in_draft_mode and self.env['account.move.line'].new or self.env['account.move.line'].create
                        dict = {
                            'move_name': self.name,
                            'name': ks_name,
                            'price_unit': amount,
                            'quantity': 1,
                            'debit':  abs(amount) if amount > 0.0 else 0.0,
                            'credit': abs(amount) if amount < 0.0 else 0.0,
                            'account_id': self.account_discount_id.id,
                            'amount_currency': amount,
                            'move_id': self._origin,
                            'date': self.date,
                            'exclude_from_invoice_tab': True,
                            'partner_id': terms_lines.partner_id.id,
                            'company_id': terms_lines.company_id.id,
                            'company_currency_id': terms_lines.company_currency_id.id,
                        }
                        if not in_draft_mode and rec.state == 'draft':
                            dict['move_id'] = rec.id

                        if in_draft_mode or rec.state == 'draft' and amount != 0:

                            if not already_exists:
                                self.line_ids += create_method(dict)
                                balance = round(sum(rec.line_ids.mapped('debit')) - sum(rec.line_ids.mapped('credit')), currency.decimal_places)
                                discount_rounding = 0.0
                                if balance != 0 and balance > -0.1 and balance < 0.1:
                                    discount_rounding = -balance if balance > 0 else abs(balance)
                                if rec.move_type == 'out_refund':
                                    discount_rounding = abs(discount_rounding) if discount_rounding < 0.0 else discount_rounding
                                rec.rounding_discount_amount = discount_rounding

    @api.model
    def _get_tax_totals(self, partner, tax_lines_data, amount_total, amount_untaxed, currency):
        res = super()._get_tax_totals(partner=partner, tax_lines_data=tax_lines_data, amount_total=amount_total, amount_untaxed=amount_untaxed, currency=currency)
        for move in self:
            if move.move_type in move_type_discount:
                lang_env = move.with_context(lang=partner.lang).env
                amount_discount_global = move.amount_discount_global
                name_discount = 'Potongan by Harga'
                formatted_discount = formatLang(lang_env, amount_discount_global, currency_obj=currency)
                if 'subtotals' in res and res['subtotals']:
                    res['subtotals'][0]['move_type'] = move.move_type
                    res['subtotals'][0]['name_discount'] = name_discount
                    res['subtotals'][0]['formatted_discount'] = formatted_discount
                    if 'groups_by_subtotal' in res and res['groups_by_subtotal']:
                        if 'Untaxed Amount' in res['groups_by_subtotal']:
                            if 'tax_group_amount' in res['groups_by_subtotal']['Untaxed Amount'][0]:
                                res['groups_by_subtotal']['Untaxed Amount'][0]['tax_group_amount'] = move.amount_tax
                                res['groups_by_subtotal']['Untaxed Amount'][0]['formatted_tax_group_amount'] = formatLang(lang_env, move.amount_tax, currency_obj=currency)
                else:
                    res['subtotals'] = [{
                        'move_type' : move.move_type,
                        'name' : 'Untaxed Amount',
                        'amount': move.amount_untaxed,
                        'formatted_amount': formatLang(lang_env, move.amount_untaxed, currency_obj=currency),
                        'name_discount': name_discount,
                        'formatted_discount': formatted_discount,
                    }]
                    res['groups_by_subtotal'] = {
                        'Untaxed Amount' : [{
                            'tax_group_name': 'Taxes', 
                            'tax_group_amount': 0.0, 
                            'tax_group_base_amount': 0.0, 
                            'formatted_tax_group_amount': 'Rp 0.0', 
                            'formatted_tax_group_base_amount': 'Rp 0.0', 
                            'tax_group_id': 1, 
                            'group_key': 'Untaxed Amount-1',
                        }]
                    }
        return res
    
    def calculate_discount_multi(self):
        for move in self:
            if move.move_type in move_type_discount:
                if move.move_type in move_type_discount and move.total_discount_amount != 0:
                    move.type_update = 'discount'
                elif move.move_type in move_type_discount and move.total_discount_amount == 0:
                    move.type_update = 'product'
                else:
                    move.type_update = ''
                currencies = move._get_lines_onchange_currency().currency_id
                if move.move_type == 'entry' or move.is_outbound():
                    sign = 1
                else:
                    sign = -1
                total = total_currency = round(-move.amount_total_inv if move.move_type == 'out_invoice' else move.amount_total_inv, currencies.decimal_places)
                price_subtotal_inv = sum(move.invoice_line_ids.mapped("price_subtotal_inv"))
                total_untaxed = total_untaxed_currency =  round(-price_subtotal_inv if move.move_type == 'out_invoice' else price_subtotal_inv, currencies.decimal_places)
                move.amount_untaxed = sign * (total_untaxed_currency if len(currencies) == 1 else total_untaxed)
                move.amount_total = sign * (total_currency if len(currencies) == 1 else total)
                move.amount_untaxed_signed = -total_untaxed
                move.amount_total_signed = abs(total) if move.move_type == 'entry' else -total
                move.amount_total_in_currency_signed = abs(move.amount_total) if move.move_type == 'entry' else -(sign * move.amount_total)
                move._recompute_universal_discount_lines()
    
    @api.depends(
        'line_ids.matched_debit_ids.debit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.matched_credit_ids.credit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        'line_ids.payment_id.state',
        'line_ids.full_reconcile_id',
        'invoice_line_ids.multi_discount',
        'invoice_line_ids.tax_ids',
        'discount_type',
        'amount_discount_line',
        'amount_discount_global',
        'total_discount_amount')
    def _compute_amount(self):
        super(AccountMove, self)._compute_amount()
        for move in self:
            if move.move_type in move_type_discount:
                move.calculate_discount_multi()
                currencies = move._get_lines_onchange_currency().currency_id
                if move.move_type == 'entry' or move.is_outbound():
                    sign = 1
                else:
                    sign = -1
                total = total_currency = round(-move.amount_total_inv if move.move_type == 'out_invoice' else move.amount_total_inv, currencies.decimal_places)
                price_subtotal_inv = sum(move.invoice_line_ids.mapped("price_subtotal_inv"))
                total_untaxed = total_untaxed_currency =  round(-price_subtotal_inv if move.move_type == 'out_invoice' else price_subtotal_inv, currencies.decimal_places)
                move.amount_untaxed = sign * (total_untaxed_currency if len(currencies) == 1 else total_untaxed)
                move.amount_total = sign * (total_currency if len(currencies) == 1 else total)
                move.amount_untaxed_signed = -total_untaxed
                move.amount_total_signed = abs(total) if move.move_type == 'entry' else -total
                move.amount_total_in_currency_signed = abs(move.amount_total) if move.move_type == 'entry' else -(sign * move.amount_total)
       
    def _check_balanced(self):
        for move in self:
            if move.move_type in move_type_discount and move.state == 'draft':
                continue
            else:
                return super()._check_balanced()

    def action_post(self):
        for move in self:
            if move.move_type in move_type_discount:
                move.calculate_discount_multi()
                tax = move.invoice_line_ids.mapped("tax_ids")
                if len(tax) > 1 and move.move_type in move_type_discount:
                    raise UserError(_("Tidak Boleh Lebih dari 1 tipe pajak"))
                jumlah_inv_line = len(move.invoice_line_ids)
                jumlah_tax = 0
                for line in move.invoice_line_ids:
                    if line.tax_ids:
                        jumlah_tax += 1
                if jumlah_inv_line != jumlah_tax and jumlah_tax != 0:
                    raise UserError(_("pajak Tidak Boleh ada yang kosong Sebagian"))
                for line in move.line_ids:
                    price_subtotal = line.price_unit_inv * line.quantity
                    if line.tax_ids and line.tax_ids[0].type_invoice == 'include' and line.price_unit_inv > 0:
                        price_subtotal = (line.price_unit_inv * line.quantity) / 1.11
                    if move.move_type == 'out_invoice':
                        if line.product_id and line.credit != price_subtotal:
                            line.revisi_line_custom()
                    else:
                        if line.product_id and line.debit != price_subtotal:
                            line.revisi_line_custom()
                move.rounding_discount_amount = 0
                check_balance = round(sum(move.line_ids.mapped('debit')) - sum(move.line_ids.mapped('credit')), 4)
                already_exists = move.line_ids.filtered(lambda line: line.account_id.id == move.account_discount_id.id)
                if len(already_exists) >= 2:
                    for x in range(len(already_exists)):
                        if x > 0:
                            already_exists[x].unlink()
                if already_exists and check_balance != 0 and check_balance > -0.1 and check_balance < 0.1:
                    discount_rounding = -check_balance if check_balance > 0 else abs(check_balance)
                    amount_discount = already_exists.debit if move.move_type == 'out_invoice' else -already_exists.credit
                    if discount_rounding < 0.0 and amount_discount < 0.0:
                        amount = amount_discount - abs(discount_rounding)
                    else:  
                        amount = amount_discount + discount_rounding
                    already_exists.sudo().update({
                        'name': ks_name,
                        'debit':  abs(amount) if amount > 0.0 else 0.0,
                        'credit': abs(amount) if amount < 0.0 else 0.0,
                        'amount_currency': abs(amount) if amount > 0.0 else amount,
                    })
        return super().action_post()

    def button_draft(self):
        self.rounding_discount_amount = 0
        res = super().button_draft()
        for move in self:
            if move.move_type in move_type_discount:
                check_balance = round(sum(move.line_ids.mapped('debit')) - sum(move.line_ids.mapped('credit')), 4)
                already_exists = move.line_ids.filtered(lambda line: line.account_id.id == move.account_discount_id.id)
                if already_exists and check_balance != 0 and check_balance > -0.1 and check_balance < 0.1:
                    discount_rounding = -check_balance if check_balance > 0 else abs(check_balance)
                    amount_discount = already_exists.debit if move.move_type == 'out_invoice' else -already_exists.credit
                    amount = amount_discount + discount_rounding
                    already_exists.update({
                        'name': ks_name,
                        'debit':  abs(amount) if amount > 0.0 else 0.0,
                        'credit': abs(amount) if amount < 0.0 else 0.0,
                        'amount_currency': abs(amount) if amount > 0.0 else amount,
                    })
        return res

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    amount_discount = fields.Float(
        'Discount Amount', default=0
    )
    multi_discount = fields.Char(
        'Discounts'
    )
    price_unit_inv = fields.Float(
        string='Unit Price', digits='Product Price'
    )
    price_subtotal_inv = fields.Monetary(
        string='Subtotal Inv', store=True, readonly=True, currency_field='currency_id'
    )

    @api.onchange('price_unit_inv')
    def _onchange_price_unit_inv(self):
        for rec in self:
            rec.price_unit = rec.price_unit_inv

    def _get_subtotal_inv(self):
        for line in self:
            if line.multi_discount:
                price_subtotal = line.price_subtotal_inv
                splited_discounts = line.multi_discount.split("+")
                for discount in splited_discounts:
                    if discount != '':
                        if ',' in discount:
                            discount = discount.replace(",",".")
                        if price_subtotal != 0 and float(discount) != 0:
                            price_subtotal -= price_subtotal * (float(discount) / 100.0)
                line.price_subtotal_inv = price_subtotal
                line.amount_discount = (line.quantity * line.price_unit) - line.price_subtotal_inv
            else:
                line.multi_discount = 0
    
    @api.onchange('quantity', 'discount', 'price_unit', 'tax_ids','multi_discount')
    def _onchange_price_subtotal(self):
        res = super()._onchange_price_subtotal()
        for line in self:
            if line.move_id.move_type in move_type_discount:
                if line.price_unit_inv == 0:
                    line.price_unit_inv = line.price_unit
                else:
                    line.price_unit = line.price_unit_inv
                line.price_subtotal_inv = line.price_unit_inv * line.quantity
                line._get_subtotal_inv()
        return res
    
    @api.model
    def _get_price_total_and_subtotal_model(self, price_unit, quantity, discount, currency, product, partner, taxes, move_type):
        res_line = super(AccountMoveLine, self)._get_price_total_and_subtotal_model(price_unit, quantity, discount, currency, product, partner, taxes, move_type)
        if move_type in move_type_discount and taxes and taxes[0].type_invoice == 'include':
            res_line['price_subtotal'] = res_line['price_total'] = (quantity * self.price_unit_inv) / 1.11
        return res_line
    
    def revisi_line_custom(self):
        for line in self:
            if line.move_id.move_type in move_type_discount:
                price_subtotal = line.price_unit_inv * line.quantity
                if line.tax_ids and line.tax_ids[0].type_invoice == 'include' and line.price_unit_inv > 0:
                    price_subtotal = (line.price_unit_inv * line.quantity) / 1.11
                line.update({
                        'price_subtotal': price_subtotal,
                        'price_total': price_subtotal,
                        'debit': 0.0  if line.move_id.move_type == 'out_invoice' else price_subtotal,
                        'credit': price_subtotal  if line.move_id.move_type == 'out_invoice' else 0.0,
                        'amount_currency': -price_subtotal if line.move_id.move_type == 'out_invoice' else price_subtotal,
                    })