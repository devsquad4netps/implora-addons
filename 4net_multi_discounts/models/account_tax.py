# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.tools.float_utils import float_round as round
from odoo.exceptions import UserError, ValidationError

import math
import logging


class AccountTax(models.Model):
    _inherit = 'account.tax'

    type_invoice = fields.Selection(
        [("include","Include"),("exclude","Exclude")], string="Type Invoice"
    )