from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import date, datetime
from odoo.exceptions import ValidationError, UserError, Warning
from dateutil.relativedelta import *

class ResCompany(models.Model):
    _inherit = 'res.company'
    
    account_discount_id = fields.Many2one(
        'account.account', 'Account Discount', default=lambda self: self.env['account.account'].search([('code','=','410030')],limit=1)
    )
class AccountReportConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    account_discount_id = fields.Many2one(
        'account.account', 'Account Discount', related='company_id.account_discount_id', readonly=False
    )