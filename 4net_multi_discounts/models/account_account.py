# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError

class AccountAccount(models.Model):
    _inherit = "account.account"

    type_discount = fields.Selection([('third_parties','Third Parties'),('related_parties','Related Parties')], 'Type Discount')