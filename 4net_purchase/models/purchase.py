from odoo import api, fields, models, _
from odoo.exceptions import AccessError, UserError, ValidationError

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    def mass_set_to_draft(self):
        for rec in self:
            if rec.state == 'draft':
                raise UserError(_(f'Doc {rec.name} Masih Status Draft'))
            if rec.invoice_count > 0:
                raise UserError(_(f'Doc {rec.name} Sudah Di lalukan vendor bill'))
            picking_ids = rec.picking_ids.filtered(lambda l: l.state == 'done')
            if picking_ids:
                raise UserError(_(f'Doc {rec.name} Sudah Di lalukan receipt'))
            rec.button_cancel()
            rec.button_draft()
    
class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    