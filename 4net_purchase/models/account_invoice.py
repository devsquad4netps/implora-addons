# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.onchange('purchase_vendor_bill_id', 'purchase_id')
    def _onchange_purchase_auto_complete(self):
        ''' Load from either an old purchase order, either an old vendor bill.

        When setting a 'purchase.bill.union' in 'purchase_vendor_bill_id':
        * If it's a vendor bill, 'invoice_vendor_bill_id' is set and the loading is done by '_onchange_invoice_vendor_bill'.
        * If it's a purchase order, 'purchase_id' is set and this method will load lines.

        /!\ All this not-stored fields must be empty at the end of this function.
        '''
        reference_picking = self.purchase_vendor_bill_id.reference
        purchase_id = self.purchase_vendor_bill_id.purchase_order_id.id
        if self.purchase_vendor_bill_id.vendor_bill_id:
            self.invoice_vendor_bill_id = self.purchase_vendor_bill_id.vendor_bill_id
            self._onchange_invoice_vendor_bill()
        elif self.purchase_vendor_bill_id.purchase_order_id:
            self.purchase_id = self.purchase_vendor_bill_id.purchase_order_id
        self.purchase_vendor_bill_id = False
        
        if not self.purchase_id:
            return
        po_line_list = []
        domain_picking = [("purchase_id",'=',self.purchase_id.id)]
        if reference_picking:
            domain_picking.append(("receipt_no","=",reference_picking))
        src_pciking = self.env['stock.picking'].search(domain_picking)

        # Copy data from PO
        invoice_vals = self.purchase_id.with_company(self.purchase_id.company_id)._prepare_invoice()
        invoice_vals['currency_id'] = self.line_ids and self.currency_id or invoice_vals.get('currency_id')
        del invoice_vals['ref']
        self.update(invoice_vals)

        # Copy purchase lines.
        # po_lines = self.purchase_id.order_line - self.line_ids.mapped('purchase_line_id')
        list_picking = []
        for line in self.line_ids:
            for pick in line.picking_ids:
                list_picking.append(pick.name)

        new_lines = self.env['account.move.line']
        for ps in src_pciking.filtered(lambda l : l.name not in list_picking):
            for sm in ps.move_ids_without_package:
                inv_line_product = self.line_ids.filtered(lambda l: l.product_id.id == sm.product_id.id)
                if not inv_line_product:
                    new_line = new_lines.new(sm.purchase_line_id._prepare_account_move_line(self))
                    new_line.update({'quantity' : sm.quantity_done, 'stock_move_id' : sm.id, 'picking_ids' : ps.ids })
                    new_line.account_id = new_line._get_computed_account()
                    new_line._onchange_price_subtotal()
                    new_lines += new_line
                else:
                    inv_line_product.update({'quantity' : inv_line_product.quantity + sm.quantity_done})
                    inv_line_product.picking_ids |= ps           
        new_lines._onchange_mark_recompute_taxes()

        # Compute invoice_origin.
        origins = set(self.line_ids.mapped('purchase_line_id.order_id.name'))
        self.invoice_origin = ','.join(list(origins))

        # Compute ref.
        refs = self._get_invoice_reference()
        self.ref = ', '.join(refs)

        # Compute payment_reference.
        if len(refs) == 1:
            self.payment_reference = refs[0]

        self.purchase_id = False
        self._onchange_currency()
        self.partner_bank_id = self.bank_partner_id.bank_ids and self.bank_partner_id.bank_ids[0]

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    stock_move_id = fields.Many2one(
        "stock.move", "Stock Move"
    )
    picking_ids = fields.Many2many(
        "stock.picking", string="Picking Ids"
    )