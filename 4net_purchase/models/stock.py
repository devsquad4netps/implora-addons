from odoo import api, fields, models, _
from odoo.tools.float_utils import float_round


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    purchase_custom_id = fields.Many2one('purchase.order', string="Purchase Orders")

    def update_line_pruchase(self):
        for picking in self:
            for line in picking.move_ids_without_package:
                pl_line = self.env['purchase.order.line'].search([('order_id','=',line.picking_id.purchase_custom_id.id),('product_id', '=',line.product_id.id)],limit=1)
                line.purchase_line_id = pl_line.id or False
                if line.purchase_line_id:
                    line.purchase_line_id._compute_qty_received()