# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Custom Purchase',
    'version'       : '1.2',
    'category'      : 'purchase',
    'sequence'      : 35,
    'summary'       : '',
    'description'   : "",
    'website'       : '',
    'depends'       : ['purchase','stock','purchase_stock','purchase_vendorbill_advance','purchase_enterprise'],
    'data'          : [
                        'security/purchase_security.xml',
                        'report/purchase_bill_views.xml',
                        'report/purchase_report_views.xml',
                        'views/purchase_views.xml',
                        'views/stock_picking_views.xml',
                        'views/account_move_views.xml',
                        'views/account_invoice_view.xml',
                      ],
    'installable'   : True,
    'auto_install'  : False,
    'application'   : True,
    'license'       : 'LGPL-3',
}
