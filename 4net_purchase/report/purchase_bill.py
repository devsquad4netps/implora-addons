# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, tools
from odoo.tools import formatLang

class PurchaseBillUnion(models.Model):
    _inherit = 'purchase.bill.union'

    qty_received = fields.Float(string='Received', readonly=True)
    qty_bill = fields.Float(string='Billed', readonly=True)
    is_create_bill = fields.Boolean("Create Bill", readonly=True)
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'purchase_bill_union')
        self.env.cr.execute("""
            CREATE OR REPLACE VIEW purchase_bill_union AS (
                SELECT
                    id, 
                    name, 
                    ref as reference, 
                    partner_id, 
                    date, 
                    amount_untaxed as amount, 
                    currency_id, 
                    company_id,
                    id as vendor_bill_id, 
                    NULL as purchase_order_id,
                    0 as qty_received,
                    0 as qty_bill,
                    false as is_create_bill
                FROM account_move
                WHERE
                    move_type='in_invoice' and state = 'posted'
            UNION
                SELECT
                    -sp.id, 
                    po.name, 
                    sp.receipt_no as reference, 
                    po.partner_id, 
                    date_order::date as date, 
                    sum(sm.product_qty * pol.price_unit) as amount,
                    po.currency_id, 
                    po.company_id,
                    am.id as vendor_bill_id,
                    po.id as purchase_order_id,
                    sum(sm.product_uom_qty) as qty_received,
                    COALESCE(sum(aml.quantity), 0) as qty_bill,
                    CASE
					    WHEN sum(sm.product_uom_qty) != COALESCE(sum(aml.quantity), 0) THEN true
					    ELSE false
				 	END 
				 	AS is_create_bill
                FROM purchase_order po
                	left join purchase_order_line pol on (po.id = pol.order_id)
                	left join stock_move sm on (pol.id = sm.purchase_line_id)
                	left join stock_picking sp ON (sm.picking_id = sp.id)
                    left join account_move_line aml on(aml.purchase_line_id = pol.id)
                    left join account_move am on(am.id = aml.move_id)
                WHERE
                    po.state in ('purchase', 'done') and sp.state = 'done'
               	group by
               		sp.id, po.name, sp.receipt_no, po.partner_id, po.currency_id, am.id, po.id
            )""")
    
    def name_get(self):
        result = []
        for doc in self:
            name = doc.name or ''
            if doc.reference:
                name += ' - ' + doc.reference
            amount = doc.amount
            if doc.purchase_order_id and doc.purchase_order_id.invoice_status == 'no':
                amount = 0.0
            name += ': ' + formatLang(self.env, amount, monetary=True, currency_obj=doc.currency_id)
            result.append((doc.id, name))
        return result