# -*- coding: utf-8 -*-
{
    'name': "Payment",
    'summary': """
        Payment
    """,
    'description': """
        Payment
    """,
    'author': "Dani R.",
    'website': "http://www.yourcompany.com",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'wizard/payment_register.xml',
        'views/payment_misc.xml',
        'views/payment.xml',
        'views/menu.xml',
    ],
}
