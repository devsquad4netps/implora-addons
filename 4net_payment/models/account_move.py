# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    payment_misc_id = fields.Many2one('account.payment.misc',
        string='Other Payment/Receipt Line', copy=False)
    
    