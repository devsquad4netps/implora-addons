# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class AccountPaymentMultiple(models.Model):
    _name = "account.payment.multiple"
    _description = "Multiple Account Payment Line"

    name = fields.Char(string='Label')
    account_id = fields.Many2one('account.account', string='Account', 
        domain=[('deprecated', '=', False)], copy=False)
    currency_id = fields.Many2one('res.currency', string='Currency', 
        default=lambda self: self.env.user.company_id.currency_id)
    payment_id = fields.Many2one('account.payment', 
        string='Payment', index=True, ondelete='cascade')
    amount = fields.Monetary(string='Amount', currency_field='currency_id')
    
class AccountPayment(models.Model):
    _inherit = "account.payment"

    write_off_line_ids = fields.One2many('account.payment.multiple', 
        inverse_name='payment_id', string='Post Difference In')

    def _prepare_move_line_default_vals(self, write_off_line_vals=None):
        ''' Prepare the dictionary to create the default account.move.lines for the current payment.
        :param write_off_line_vals: Optional dictionary to create a write-off account.move.line easily containing:
            * amount:       The amount to be added to the counterpart amount.
            * name:         The label to set on the line.
            * account_id:   The account on which create the write-off.
        :return: A list of python dictionary to be passed to the account.move.line's 'create' method.
        '''
        self.ensure_one()
        write_off_line_vals = write_off_line_vals or {}

        if not self.outstanding_account_id:
            raise ValidationError(_("""
                You can't create a new payment without an outstanding payments/receipts 
                account set either on the company or the %s payment method in the %s journal.
            """, self.payment_method_line_id.name, self.journal_id.display_name))

        # Compute amounts.
        write_off_amount_currency = write_off_line_vals.get('amount', 0.0)
        
        if self.payment_type == 'inbound':
            # Receive money.
            liquidity_amount_currency = self.amount
        elif self.payment_type == 'outbound':
            # Send money.
            liquidity_amount_currency = -self.amount
            write_off_amount_currency *= -1
        else:
            liquidity_amount_currency = write_off_amount_currency = 0.0

        write_off_balance = self.currency_id._convert(
            write_off_amount_currency,
            self.company_id.currency_id,
            self.company_id,
            self.date,
        )
        liquidity_balance = self.currency_id._convert(
            liquidity_amount_currency,
            self.company_id.currency_id,
            self.company_id,
            self.date,
        )


        # * =============================================================================================
        write_off_amount = sum(x.amount for x in self.write_off_line_ids)
        if self.payment_type == 'inbound':
            write_off_amount = -(write_off_amount)
        # print("### write_off_amount ###", write_off_amount)
        # * =============================================================================================
        
        counterpart_amount_currency = -liquidity_amount_currency
        counterpart_balance = -liquidity_balance - write_off_balance
        currency_id = self.currency_id.id
    
        # print("### counterpart_balance ###", counterpart_balance)

        if self.is_internal_transfer:
            if self.payment_type == 'inbound':
                liquidity_line_name = _('Transfer to %s', self.journal_id.name)
            else: # payment.payment_type == 'outbound':
                liquidity_line_name = _('Transfer from %s', self.journal_id.name)
        else:
            liquidity_line_name = self.payment_reference

        # Compute a default label to set on the journal items.
        payment_display_name = {
            'outbound-customer': _("Customer Reimbursement"),
            'inbound-customer': _("Customer Payment"),
            'outbound-supplier': _("Vendor Payment"),
            'inbound-supplier': _("Vendor Reimbursement"),
        }

        default_line_name = self.env['account.move.line']._get_default_line_name(
            _("Internal Transfer") if self.is_internal_transfer else payment_display_name['%s-%s' % (self.payment_type, self.partner_type)],
            self.amount,
            self.currency_id,
            self.date,
            partner=self.partner_id,
        )

        line_vals_list = [
            # Liquidity line.
            {
                'name': liquidity_line_name or default_line_name,
                'date_maturity': self.date,
                'amount_currency': liquidity_amount_currency,
                'currency_id': currency_id,
                'debit': liquidity_balance if liquidity_balance > 0.0 else 0.0,
                'credit': -liquidity_balance if liquidity_balance < 0.0 else 0.0,
                'partner_id': self.partner_id.id,
                'account_id': self.outstanding_account_id.id,
            },
            # Receivable / Payable.
            {
                'name': self.payment_reference or default_line_name,
                'date_maturity': self.date,
                'amount_currency': counterpart_amount_currency,
                'currency_id': currency_id,
                'debit': counterpart_balance if counterpart_balance > 0.0 else 0.0,
                'credit': -counterpart_balance if counterpart_balance < 0.0 else 0.0,
                'partner_id': self.partner_id.id,
                'account_id': self.destination_account_id.id,
            },
        ]

        # * multiple account write off
        if not self.currency_id.is_zero(write_off_amount_currency):
            # * ===================================================================
            for wo in self.write_off_line_ids:
                if not self.payment_type:
                    raise ValidationError('Undefined Payment Type !')
                    

                debit_amount = 0
                credit_amount = 0
                write_off_amount = wo.amount

                if self.payment_type == 'outbound':
                    if write_off_amount < 0.0:
                        debit_amount = abs(write_off_amount)
                        credit_amount = 0
                    else:
                        debit_amount = 0
                        credit_amount = abs(write_off_amount)
                else:
                    if write_off_amount < 0.0:
                        debit_amount = 0
                        credit_amount = abs(write_off_amount)
                    else:
                        debit_amount = abs(write_off_amount)
                        credit_amount = 0

                line_name = 'Write Off - %s - %s' % \
                    (str(wo.account_id.name or ''), str( self.partner_id.name or ''))
                line_vals_list.append({
                    'name': line_name,
                    'debit': debit_amount,
                    'credit': credit_amount,
                    'currency_id': currency_id,
                    'account_id': wo.account_id.id,
                    'partner_id': self.partner_id.id,
                    'amount_currency' : debit_amount if debit_amount > 0 else credit_amount,
                })
            # * ===================================================================

        # * debugging
        # for lvl in line_vals_list:
        #     print("\n ### line_vals_list ###", lvl)
        # print("### sum debit ###", sum(x['debit'] for x in line_vals_list))
        # print("### sum credit ###", sum(x['credit'] for x in line_vals_list))
        # print(stopx)
        # * =========

        return line_vals_list
