# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class AccountPayment(models.Model):
    _inherit = "account.payment"

    is_misc_payment = fields.Boolean()
    misc_line_ids = fields.One2many('account.payment.misc', 
        inverse_name='payment_id', 
        string='Miscellanous Payments Line')

    @api.depends('misc_line_ids')
    def _compute_sum_misc_line_ids(self):
        sumx = 0
        negative_amount = False
        for line in self.misc_line_ids:
            sumx += line.amount
            if line.amount < 0:
                negative_amount = True
        self.sum_misc_line_ids = sumx
        self.negative_amount = negative_amount

    negative_amount = fields.Boolean(compute='_compute_sum_misc_line_ids')
    sum_misc_line_ids = fields.Monetary(compute='_compute_sum_misc_line_ids')

    def _prepare_move_line_default_vals(self, write_off_line_vals=None):
        res = super(AccountPayment, self)._prepare_move_line_default_vals(write_off_line_vals)
        if self.is_misc_payment:
            if not self.misc_line_ids:
                raise ValidationError('Payment Lines should not Empty !')

            # print("### amount header ###", self.amount)
            # print("### amount lines ###", self.sum_misc_line_ids)
            # if self.amount != self.sum_misc_line_ids:
            #     raise ValidationError('Amount Total Payment Lines must be equal to Amount !')
            
            res[0]['name'] = self.outstanding_account_id.name
            del res[1]
            if not self.negative_amount:
                for line in self.misc_line_ids:
                    if line.amount == 0:
                        raise ValidationError('Amount should not zero !')
                    res.append({
                        'name': line.account_id.name,
                        'date_maturity': self.date,
                        'amount_currency': line.amount,
                        'currency_id': self.currency_id.id,
                        'debit': abs(line.amount) if self.payment_type == 'outbound' else 0,
                        'credit': abs(line.amount) if self.payment_type == 'inbound' else 0,
                        'partner_id': self.partner_id.id,
                        'account_id': line.account_id.id,
                        'payment_misc_id': line.id,
                    })
            else:
                nml_vals = self._prepare_negative_move_line_vals(res)
                res = nml_vals
        # for r in res:
        #     print("### ", 'Name:' + r['name'], 'Debit: ' + str(r['debit']), 'Credit: '+ str(r['credit']))
        # print(stopx)
        return res

    def _prepare_negative_move_line_vals(self, res):
        for line in self.misc_line_ids:
            if line.amount == 0:
                raise ValidationError('Amount should not zero !')
            debit_amount = abs(line.amount) if self.payment_type == 'inbound' else 0
            credit_amount = abs(line.amount) if self.payment_type == 'outbound' else 0
            if line.amount > 0:
                debit_amount = abs(line.amount) if self.payment_type == 'outbound' else 0
                credit_amount = abs(line.amount) if self.payment_type == 'inbound' else 0
            res.append({
                'name': line.account_id.name,
                'date_maturity': self.date,
                'amount_currency': line.amount,
                'currency_id': self.currency_id.id,
                'debit': debit_amount,
                'credit': credit_amount,
                'partner_id': self.partner_id.id,
                'account_id': line.account_id.id,
                'payment_misc_id': line.id,
            })
        return res

    @api.depends('move_id.name')
    def name_get(self):
        res = super(AccountPayment, self).name_get()
        for record in self:
            if record.is_misc_payment and record.payment_type:
                type_dict = {
                    'inbound': 'Draft Other Receipt',
                    'outbound': 'Draft Other Payment',
                }
                res.append((
                    record.id, 
                    record.move_id.name != '/' and record.move_id.name 
                    or type_dict.get(record.payment_type)
                ))
        return res

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        res = super(AccountPayment, self).copy()
        if res.is_misc_payment:
            raise ValidationError('This transaction cannot be duplicated !')
        return res

    fixed_payment_type = fields.Char()
    
    @api.onchange('fixed_payment_type', 'payment_type')
    def _onchange_payment_type(self):
        value = {}
        if self.fixed_payment_type:
            value['payment_type'] = self.fixed_payment_type
        return {'value': value}
    
    @api.depends('amount_total_signed', 'payment_type')
    def _compute_amount_company_currency_signed(self):
        for payment in self:
            liquidity_lines = payment._seek_for_lines()[0]
            result = sum(liquidity_lines.mapped('balance')) 
            if payment.is_misc_payment:
                result = payment.amount
            # print("### result ###", result)
            payment.amount_company_currency_signed = result

    def write(self, vals):
        res = super(AccountPayment, self).write(vals)
        self._sync_to_moves(set(vals.keys()))
        return res

    def _sync_to_moves(self, changed_fields):
        # print("### changed_fields ###", changed_fields)
        if not any(field_name in changed_fields for field_name in 
            ('date', 'amount', 'payment_type', 'partner_type', 
            'payment_reference', 'is_internal_transfer', 
            'currency_id', 'partner_id', 'destination_account_id', 
            'partner_bank_id', 'journal_id', 'misc_line_ids')):
            return
        for pay in self:
            if pay.is_misc_payment:
                line_ids = [(0, 0, line_vals) for line_vals in pay._prepare_move_line_default_vals()]
                if line_ids:
                    pay.move_id.line_ids.unlink()
                pay.move_id.write({
                    'partner_id': pay.partner_id.id,
                    'currency_id': pay.currency_id.id,
                    'partner_bank_id': pay.partner_bank_id.id,
                    'line_ids': line_ids,
                })
        return

class AccountPaymentMisc(models.Model):
    _check_company_auto = True
    _name = 'account.payment.misc'
    _description = 'Miscellanous Payments'

    name = fields.Char(string='Label')
    account_id = fields.Many2one('account.account', string='Account', 
        domain=[('deprecated', '=', False)], copy=False)
    currency_id = fields.Many2one('res.currency', string='Currency', 
        default=lambda self: self.env.user.company_id.currency_id)
    payment_id = fields.Many2one('account.payment', 
        string='Payment', index=True, ondelete='cascade')
    amount = fields.Monetary(string='Amount', currency_field='currency_id')
    
    @api.onchange('account_id')
    def _onchange_account_id(self):
        value = {}
        if self.account_id:
            value['name']= self.account_id.name
        return {'value': value}
        