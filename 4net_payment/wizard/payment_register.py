# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class AccountPaymentRegisterMultiple(models.TransientModel):
    _name = "account.payment.register.multiple"
    _description = "Multiple Account Payment Register Line"

    name = fields.Char(string='Label')
    account_id = fields.Many2one('account.account', string='Account', 
        domain=[('deprecated', '=', False)], copy=False)
    currency_id = fields.Many2one('res.currency', string='Currency', 
        default=lambda self: self.env.user.company_id.currency_id)
    payment_register_id = fields.Many2one('account.payment.register', 
        string='Payment Register', index=True, ondelete='cascade')
    amount = fields.Monetary(string='Amount', currency_field='currency_id')
    
    @api.onchange('account_id')
    def _onchange_account_id(self):
        value = {}
        if self.account_id:
            value['name']= self.account_id.name
        return {'value': value}

class AccountRegisterPayment(models.TransientModel):
    _inherit = "account.payment.register"

    @api.depends('account_multiple_ids')
    def _compute_sum_account_multiple_ids(self):
        sum = 0
        for data in self.account_multiple_ids:
            sum += data.amount
        self.sum_account_multiple_ids = sum

    account_multiple_ids = fields.One2many('account.payment.register.multiple', 
        inverse_name='payment_register_id', string='Post Difference In')
    sum_account_multiple_ids = fields.Monetary(compute='_compute_sum_account_multiple_ids', 
        readonly=True, string='Post Difference In Total Amount')

    def _check_pay_diff_handling(self):
        payments = False
        if self.payment_difference_handling != 'open':
            if self.payment_difference != self.sum_account_multiple_ids:
                msg = "Total Amount 'Post Difference' must be equal to 'Payment Difference' !"
                raise ValidationError(msg)
            payments = self.with_context(skip_account_move_synchronization=True)._create_payments()
        else:
            payments = self._create_payments()
        return payments
    
    def action_create_payments(self):
        # * ===================================================================================
        payments = self._check_pay_diff_handling()
        if not payments:
            raise ValidationError('Something went wrong, payment not created !')
        # * ===================================================================================

        print("### ctx ###", self._context)
        if self._context.get('dont_redirect_to_payments'):
            return True

        action = {
            'name': _('Payments'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment',
            'context': {'create': False},
        }
        print("### action ###", action)
        
        # print(stopx)

        if len(payments) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': payments.id,
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('id', 'in', payments.ids)],
            })
        return action

    def _create_payment_vals_from_wizard(self):
        res = super(AccountRegisterPayment, self)._create_payment_vals_from_wizard()
        list_ids = []
        for acc in self.account_multiple_ids:
            # if acc.amount <= 0:
            #     msg = "Amount 'Post Difference' must be greater than 0 !"
            #     raise ValidationError(msg)
            vals = ((0, 0, {
                'name': acc.name,
                'amount': acc.amount,
                'account_id': acc.account_id.id,
            }))
            list_ids.append(vals)
        res['write_off_line_ids'] = list_ids
        # print("### res _create_payment_vals_from_wizard ###", res)
        return res
    
