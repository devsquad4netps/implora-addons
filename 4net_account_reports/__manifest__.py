# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name'          : 'Custom Accounting Reports',
    'summary'       : 'View and create reports',
    'category'      : 'Accounting/Accounting',
    'description'   : """ Accounting Reports ================== """,
    'depends'       : ['account_accountant','account_reports','account_parent'],
    'data'          : [
                        # 'security/ir.model.access.csv',
                        'data/account_financial_report_data.xml',
                        'views/account_financial_report_view.xml',
                        'views/report_pl_and_bs.xml',
                        'views/report_gl_and_tb.xml',
                        'views/report_coa_hierarchy.xml',
                        'wizard/open_chart.xml',
                      ],
    'auto_install': True,
    'installable': True,
    'license': 'OEEL-1',
}
