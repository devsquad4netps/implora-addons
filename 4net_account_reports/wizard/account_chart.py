from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval
from datetime import datetime
import time
from odoo.exceptions import UserError
from markupsafe import Markup
from odoo.tools.misc import formatLang, format_date, get_lang
from dateutil.relativedelta import relativedelta


class OpenAccountChart(models.TransientModel):
    _inherit = "account.open.chart"

    type_comparison = fields.Selection(
        [("previous", "Previous"),("last_year", "Last Year")], string="Type Comparison"
    )
    previous_period = fields.Integer(
        string='Previous Period'
    )
    period_last_year = fields.Integer(
        string='Same Period Last Year'
    )

    def comparison_year(self, date, number):
        if number == 0:
            number = 1
        no = 1
        data_dict = {}
        data_list = []
        date_date = date
        for x in range(number):
            data_dict[date_date] = {'debit': [], 'credit' : [], 'balance' : []}
            data_list.append(date_date)
            date_date = date_date - relativedelta(years=1)
            no += 1
        return data_dict, date_date
        

    def _build_contexts(self):
        res_report = super(OpenAccountChart, self)._build_contexts()
        date_now = datetime.today().date()
        if self.date_to:
            date_to = self.date_to
        else:
            date_to = date_now
        comparison_year = {}
        comparison_year_list = []
        res_report['date_to'] = date_to
        res_report['previous_period'] = self.previous_period or 0
        res_report['period_last_year'] = self.period_last_year or 0
        comparison_number = self.period_last_year
        comparison_year, comparison_year_list = self.comparison_year(date_to, comparison_number)
        res_report['comparison_year'] = comparison_year_list
        return res_report

    def line_data(self, level, parent_id, wiz_id=False, account=False):
        res = super().line_data(level, parent_id, wiz_id, account)
        res['debit'] = formatLang(self.env, account.debit, currency_obj=account.company_id.currency_id)
        res['credit'] = formatLang(self.env, account.credit, currency_obj=account.company_id.currency_id)
        res['balance'] = formatLang(self.env, account.balance, currency_obj=account.company_id.currency_id)
        res['initial_balance'] = formatLang(self.env, account.initial_balance, currency_obj=account.company_id.currency_id)
        res['ending_balance'] = formatLang(self.env, account.initial_balance + account.balance, currency_obj=account.company_id.currency_id)
        return res
        
    def at_line_data(self, at_data, level, wiz_id=False, parent_id=False, accounts=False):
        res = super().at_line_data(at_data, level, wiz_id, parent_id, accounts)
        if not accounts:
            accounts = self.env['account.account'].browse()
        total_credit = sum(accounts.mapped('credit'))
        total_debit = sum(accounts.mapped('debit'))
        total_balance = sum(accounts.mapped('balance'))
        total_initial_balance = sum(accounts.mapped('initial_balance'))
        total_ending_balance = total_initial_balance + total_balance
        res['debit'] = formatLang(self.env, total_debit, currency_obj=accounts.company_id.currency_id)
        res['credit'] = formatLang(self.env, total_credit, currency_obj=accounts.company_id.currency_id)
        res['balance'] = formatLang(self.env, total_balance, currency_obj=accounts.company_id.currency_id)
        res['initial_balance'] = formatLang(self.env, total_initial_balance, currency_obj=accounts.company_id.currency_id)
        res['ending_balance'] = formatLang(self.env, total_ending_balance, currency_obj=accounts.company_id.currency_id)
        return res