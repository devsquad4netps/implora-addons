

# -*- coding: utf-8 -*-

import io
import base64
import markupsafe
from dateutil import parser
from odoo.tools.misc import xlsxwriter
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from odoo.addons.account_reports.models.formula import FormulaSolver

class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    def _export_xlsx_pnl_bs(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {
            'in_memory': True,
            'strings_to_formulas': False,
        })
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        string_model = str(self)

        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})

        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style_header = workbook.add_format({'font_name': 'Arial', 'bold': True})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        # title_style.set_align('center')
        title_style.set_align('vcenter')
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})

        # * set border
        default_col1_style.set_border(1)
        default_style.set_border(1)
        title_style.set_border(1)
        level_0_style.set_border(1)
        level_1_style.set_border(1)
        level_2_style.set_border(1)
        level_2_col1_style.set_border(1)
        level_2_col1_total_style.set_border(1)
        level_3_style.set_border(1)
        level_3_col1_style.set_border(1)
        level_3_col1_total_style.set_border(1)

        # * set the column width
        sheet.set_column(0, 0, 35)
        sheet.set_column(1, 1, 21)
        sheet.set_column(2, 2, 51)
        sheet.set_column(3, 3, 21)

        y_offset = 5
        headers, lines = self.with_context(no_format=True, print_mode=True, prefetch_fields=False)._get_table(options)

        # header
        date_from = parser.parse(options['date']['date_from']).strftime("%d %b %Y")
        date_to = parser.parse(options['date']['date_to']).strftime("%d %b %Y")
        period = f'Period {date_from} - {date_to}'

        sheet.write(0, 0, self.env.company.name, title_style_header)
        sheet.write(1, 0, sheet.name, title_style_header)
        sheet.write(2, 0, period, title_style_header)
        # image
        image_company = io.BytesIO(base64.b64decode(self.env.company.logo))
        sheet.insert_image('D1', 'image.png', {'image_data': image_company, 'x_scale': 0.5, 'y_scale': 0.4})

        # * add new headers.
        # print("### headers ###", headers)
        new_headers = [[
            {'name': '', 'class': 'string', 'rowspan': 2}, 
            {'name': 'Kode Account', 'class': 'string', 'rowspan': 2}, 
            {'name': 'Nama Account', 'class': 'string', 'rowspan': 2}, 
            {'name': '', 'class': 'string', 'rowspan': 2}, 
        ]]
        headers = new_headers
        for header in headers:
            x_offset = 0
            for column in header:
                column_name_formated = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                rowspan = column.get('rowspan', 1)
                if rowspan == 1:
                    sheet.write(y_offset, x_offset, column_name_formated, title_style)
                else:
                    sheet.merge_range(y_offset, x_offset, y_offset + rowspan - 1, x_offset, column_name_formated, title_style)
                x_offset += 1
            y_offset += 1

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        # * Add lines.
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            # * write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                if 'unfoldable' not in lines[y]:
                    # print("### lines y ###", lines[y])
                    target_id = self._get_caret_option_target_id(lines[y].get('id', 0))
                    account_id = self.env[lines[y]['caret_options']].browse(target_id)
                    # sheet.write(y + y_offset, 1, cell_value, col1_style)
                    # default_style.set_align('center')
                    sheet.write(y + y_offset, 1, account_id.code, default_style)
                    sheet.write(y + y_offset, 2, account_id.name, default_style)
                else:
                    # print("### lines y ###", lines[y])
                    # if lines[y]['level'] in [0, 1, 2]:
                    sheet.write(y + y_offset, 0, cell_value, col1_style)
                    sheet.write(y + y_offset, 1, ' ', col1_style)
                    sheet.write(y + y_offset, 2, ' ', col1_style)

            # * write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, 3, cell_value, date_default_style)
                    # sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, 3, cell_value, style)
                    # sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()

        return generated_file

    def get_html(self, options, line_id=None, additional_context=None):
        res = super(AccountReport, self).get_html(options, line_id, additional_context)
        if self._name == 'account.financial.html.report' and self.env.context.get('print_mode'):
            date_from = parser.parse(options['date']['date_from']).strftime("%d %b %Y")
            date_to = parser.parse(options['date']['date_to']).strftime("%d %b %Y")
            period = f'Period {date_from} - {date_to}'
            options['financial_report_period'] = period
            options['company_id'] = self.env.company
            # print("### options company_id ###", options['company_id'])
            return self._compute_html(options, line_id, additional_context)
        return res

    def _compute_html(self, options, line_id=None, additional_context=None):
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        templates['main_template'] = '4net_account_reports.pl_and_bs_main_template'
        report_manager = self._get_report_manager(options)

        render_values = self._get_html_render_values(options, report_manager)
        if additional_context:
            render_values.update(additional_context)

        # Create lines/headers.
        if line_id:
            headers = options['headers']
            lines = self._get_lines(options, line_id=line_id)
            template = templates['line_template']
        else:
            headers, lines = self._get_table(options)
            template = templates['main_template']
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        lines = self._format_lines_for_display(lines, options)
        
        columns_header = []
        for header in headers:
            for head in header:
                head.update({'name': ''})
            header.append({'name': ''})
            # header.append({'name': ''})
            columns_header.append(header)

        new_lines = []
        new_lines.append({
            'name': '',
            'level': 1, 
            'columns': [
                {'name': 'Kode Account'},
                {'name': 'Nama Account'},
                # {'name': ''},
            ],
            'id': 'first_row',
            'style': ' border-width: 3px; font-weight: bold; border-bottom-style: double;'
        })
        for line in lines:
            if line.get('caret_options'):
                target_id = self._get_caret_option_target_id(line.get('id', 0))
                account_id = self.env[line.get('caret_options')].browse(target_id)
                line['name'] = ''
                columns = [
                    {'name': account_id.code},
                    {'name': account_id.name},
                    # {'name': ''},
                ]
                line['columns'] = columns
                new_lines.append(line)
            else:
                old_columns = line.get('columns')
                new_columns = [{'name': ''}]
                line['columns'] = new_columns + old_columns
                new_lines.append(line)
        
        render_values['lines'] = {'columns_header': columns_header, 'lines': new_lines}

        # Manage footnotes.
        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        # Render.
        html = self.env.ref(template)._render(render_values)
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(markupsafe.Markup('<div class="js_account_report_footnotes"></div>'), self.get_html_footnotes(footnotes_to_render))
        return html

    def _get_templates(self):
        return {
                'main_template': 'account_reports.main_template',
                'main_table_header_template': 'account_reports.main_table_header',
                'line_template': 'account_reports.line_template',
                'footnotes_template': 'account_reports.footnotes_template',
                'search_template': 'account_reports.search_template',
                'line_caret_options': 'account_reports.line_caret_options',
                'pdf_line_template': '4net_account_reports.pdf_line_template',
        }

    filter_hide_if_zero = None

    def _init_filter_hide_if_zero(self, options, previous_options=None):
        if self.filter_hide_if_zero is not None:
            if previous_options and 'hide_if_zero' in previous_options:
                options['hide_if_zero'] = previous_options['hide_if_zero']
            else:
                options['hide_if_zero'] = self.filter_hide_if_zero
                
class ReportAccountFinancialReport(models.Model):
    _inherit = "account.financial.html.report"

    filter_hide_if_zero = False

    def _build_lines_hierarchy(self, options_list, financial_lines, solver, groupby_keys):
        lines = []
        for financial_line in financial_lines:
            is_leaf = solver.is_leaf(financial_line)
            has_lines = solver.has_move_lines(financial_line)

            # print("### options_list hide_if_zero ###", options_list[0].get('hide_if_zero'))
            financial_report_line = self._get_financial_line_report_line(
                options_list[0],
                financial_line,
                solver,
                groupby_keys,
            )

            # ! Manage 'hide_if_zero' field.
            # if financial_line.hide_if_zero and all(self.env.company.currency_id.is_zero(column['no_format'])
            #         for column in financial_report_line['columns'] if 'no_format' in column):
            #     continue
            # * get hide_if_zero from options_list
            if options_list[0].get('hide_if_zero') and all(self.env.company.currency_id.is_zero(column['no_format'])
                    for column in financial_report_line['columns'] if 'no_format' in column):
                continue

            # Manage 'hide_if_empty' field.
            if financial_line.hide_if_empty and is_leaf and not has_lines:
                continue

            lines.append(financial_report_line)

            aml_lines = []
            if financial_line.children_ids:
                # Travel children.
                lines += self._build_lines_hierarchy(options_list, financial_line.children_ids, solver, groupby_keys)
            elif is_leaf and financial_report_line['unfolded']:
                # Fetch the account.move.lines.
                solver_results = solver.get_results(financial_line)
                sign = solver_results['amls']['sign']
                for groupby_id, display_name, results in financial_line._compute_amls_results(options_list, self, sign=sign):
                    aml_lines.append(self._get_financial_aml_report_line(
                        options_list[0],
                        financial_report_line['id'],
                        financial_line,
                        groupby_id,
                        display_name,
                        results,
                        groupby_keys,
                    ))
            lines += aml_lines

            if self.env.company.totals_below_sections and (financial_line.children_ids or (is_leaf and financial_report_line['unfolded'] and aml_lines)):
                lines.append(self._get_financial_total_section_report_line(options_list[0], financial_report_line))
                financial_report_line["unfolded"] = True  # enables adding "o_js_account_report_parent_row_unfolded" -> hides total amount in head line as it is displayed later in total line

        return lines
