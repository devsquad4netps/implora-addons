import ast
import copy
import datetime
import io
import json
import base64
import logging
import markupsafe
from dateutil import parser
from collections import defaultdict
from math import copysign, inf

import lxml.html
from babel.dates import get_quarter_names
from dateutil.relativedelta import relativedelta
from markupsafe import Markup

from odoo import models, fields, api, _
from odoo.addons.web.controllers.main import clean_action
from odoo.exceptions import RedirectWarning
from odoo.osv import expression
from odoo.tools import config, date_utils, get_lang
from odoo.tools.misc import formatLang, format_date
from odoo.tools.misc import xlsxwriter

_logger = logging.getLogger(__name__)

class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    def get_html(self, options, line_id=None, additional_context=None):
        res = super(AccountReport, self).get_html(options, line_id, additional_context)
        if self.env.context.get('print_mode'):
            if self._name == 'account.general.ledger' or self._name == 'account.coa.report':
                date_from = parser.parse(options['date']['date_from']).strftime("%d %b %Y")
                date_to = parser.parse(options['date']['date_to']).strftime("%d %b %Y")
                period = f'Period {date_from} - {date_to}'
                options['financial_report_period'] = period
                options['company_id'] = self.env.company
                return self._compute_html_new(options, line_id, additional_context)
        return res
    
    def _compute_html_new(self, options, line_id=None, additional_context=None):
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        templates['main_template'] = '4net_account_reports.gl_and_tb_main_template'
        report_manager = self._get_report_manager(options)

        render_values = self._get_html_render_values(options, report_manager)
        if additional_context:
            render_values.update(additional_context)

        # Create lines/headers.
        if line_id:
            headers = options['headers']
            lines = self._get_lines(options, line_id=line_id)
            template = templates['line_template']
        else:
            headers, lines = self._get_table(options)
            options['headers'] = headers
            template = templates['main_template']
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        lines = self._format_lines_for_display(lines, options)
        headers[0][0]['rowspan'] = 2
        headers[0][1]['rowspan'] = 2
        render_values['lines'] = {'columns_header': headers, 'lines': lines}

        # Manage footnotes.
        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        # Render.
        html = self.env.ref(template)._render(render_values)
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(markupsafe.Markup('<div class="js_account_report_footnotes"></div>'), self.get_html_footnotes(footnotes_to_render))
        return html
    
    def get_xlsx_new(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {
            'in_memory': True,
            'strings_to_formulas': False,
        })
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        string_model = str(self)

        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style_header = workbook.add_format({'font_name': 'Arial', 'bold': True })
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'border': True, 'bottom': 1,'top' : 1})
        title_style.set_align('center')
        title_style.set_align('vcenter')
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'border': True, 'bottom': 1,'top' : 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'border': True, 'bottom': 1,'top' : 1, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'border': True, 'bottom': 1,'top' : 1})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'border': True, 'bottom': 1,'top' : 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})

        #Set the first column width to 50
        sheet.set_column(0, 0, 34)
        sheet.set_column(1, 1, 37)
        sheet.set_column(2, 2, 10)
        sheet.set_column(3, 3, 40)
        sheet.set_column(4, 4, 15)
        sheet.set_column(5, 5, 15)
        sheet.set_column(6, 6, 15)
        sheet.set_column(7, 7, 15)
        sheet.set_column(8, 8, 15)

        y_offset = 5
        headers, lines = self.with_context(no_format=True, print_mode=True, prefetch_fields=False)._get_table(options)

        # header
        date_from = parser.parse(options['date']['date_from']).strftime("%d %b %Y")
        date_to = parser.parse(options['date']['date_to']).strftime("%d %b %Y")
        period = f'Period {date_from} - {date_to}'

        sheet.write(0, 0, self.env.company.name, title_style_header)
        sheet.write(1, 0, sheet.name, title_style_header)
        sheet.write(2, 0, period, title_style_header)
        # image
        image_company = io.BytesIO(base64.b64decode(self.env.company.logo))
        sheet.insert_image('H1', 'image.png', {'image_data': image_company, 'x_scale': 0.5, 'y_scale': 0.4})

        # Add headers.
        for header in headers:
            x_offset = 0
            for column in header:
                column_name_formated = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                colspan = column.get('colspan', 1)
                if colspan == 1:
                    if column_name_formated in ('Account','Nama Account') and 'account.coa.report'in string_model:
                        sheet.merge_range('A6:A7', 'Account', title_style)
                        sheet.merge_range('B6:B7', 'Nama Account', title_style)
                    else:
                        sheet.write(y_offset, x_offset, column_name_formated, title_style)
                else:
                    sheet.merge_range(y_offset, x_offset, y_offset, x_offset + colspan - 1, column_name_formated, title_style)
                x_offset += colspan
            y_offset += 1

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        # Add lines.
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)

            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
                    if lines[y]['name'] == 'Total' and lines[y]['id'] == 'general_ledger_total_1':
                        sheet.write(y + y_offset, 1, '', style)
                        sheet.write(y + y_offset, 2, '', style)
                        sheet.write(y + y_offset, 3, '', style)
                        sheet.write(y + y_offset, 4, '', style)
                        sheet.write(y + y_offset, 5, '', style)
                    if lines[y]['name'] == 'Total' and lines[y]['id'] == 'grouped_accounts_total--':
                        sheet.write(y + y_offset, 1, '', style)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()

        return generated_file

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {
            'in_memory': True,
            'strings_to_formulas': False,
        })
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        
        record_pnl = self.env.ref('account_reports.account_financial_report_profitandloss0')
        record_bs = self.env.ref('account_reports.account_financial_report_balancesheet0')
        record_coa_hierarchy = self.env.ref('4net_account_reports.account_financial_coa_base_on_accounts_hierarchy')

        string_model = str(self)
        if 'account.general.ledger' in string_model or 'account.coa.report' in string_model:
            return self.get_xlsx_new(options=options, response=response)
        elif self == record_pnl or self == record_bs:
            return self._export_xlsx_pnl_bs(options=options, response=response)
        elif self == record_coa_hierarchy:
            return self.get_xlsx_hierarchy(options=options, response=response)
        if self.id == 6 or self.name == 'CoA Based on PL':
            return self.get_xlsx_pnl_hierarchy(options=options, response=response)
        else:
            return super().get_xlsx(options=options, response=response)

        