from odoo import models, api, fields, _
from odoo.tools.misc import format_date

from dateutil.relativedelta import relativedelta
from itertools import chain

from odoo.osv import expression

from collections import defaultdict, namedtuple

from odoo.addons.account_reports.models.formula import FormulaSolver

class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    filter_individual_supplier = None
    filter_company_supplier = None

class ReportAccountAgedPartner(models.AbstractModel):
    _inherit = "account.aged.payable"
    
    def _get_columns_name(self, options):
        res = super()._get_columns_name(options=options)
        res[0]['name'] = "Bill No"
        res[0]['class'] = "text-nowrap text-center"
        res[0]['style'] = 'width: 10%; border: 1px solid #ddd;'
        # print("### res _get_columns_name ### \n", res)
        return res

    @api.model
    def _get_column_details(self, options):
        columns = [
            self._header_column(),
            self._field_column('move_ref', name="Bill Reference"),
            self._field_column('invoice_date', name=_("Invoice Date")),
            self._field_column('report_date'),
            self._field_column('amount_currency'),
            self._field_column('currency_id'),
            self._field_column('account_name', name=_("Account"), ellipsis=True),
            self._field_column('expected_pay_date'),
            self._field_column('period0', name=_("As of: %s", format_date(self.env, options['date']['date_to']))),
            self._field_column('period1', sortable=True),
            self._field_column('period2', sortable=True),
            self._field_column('period3', sortable=True),
            self._field_column('period4', sortable=True),
            self._field_column('period5', sortable=True),
            self._custom_column(  # Avoid doing twice the sub-select in the view
                name=_('Total'),
                classes=['number'],
                formatter=self.format_value,
                getter=(lambda v: v['period0'] + v['period1'] + v['period2'] + v['period3'] + v['period4'] + v['period5']),
                sortable=True,
            ),
        ]

        # if self.user_has_groups('base.group_multi_currency'):
        #     columns[3:3] = [
        #         self._field_column('move_ref',name="Bill Reference"),
        #         self._field_column('amount_currency'),
        #         self._field_column('currency_id'),
        #     ]
        return columns

    invoice_date = fields.Date(group_operator='max', string='Invoice Date')
    filter_individual_supplier = False
    filter_company_supplier = False

    def _init_filter_supplier_type(self, options, previous_options=None):
        if self.filter_individual_supplier is not None:
            if previous_options and 'individual_supplier' in previous_options:
                options['individual_supplier'] = previous_options['individual_supplier']
        if self.filter_company_supplier is not None:
            if previous_options and 'company_supplier' in previous_options:
                options['company_supplier'] = previous_options['company_supplier']

    @api.model
    def _get_sql(self):
        options = self.env.context['report_options']

        supplier_type_dict = {
            options['company_supplier'] is not False and \
                options['individual_supplier'] is False: "AND partner.is_company IS TRUE",

            options['individual_supplier'] is not False and \
                options['company_supplier'] is False: "AND partner.is_company IS NOT TRUE",
        }
        # print("### supp_type_dict ###", supplier_type_dict.get(True) or "1=1")

        query = ("""
            SELECT
                {move_line_fields},
                account_move_line.amount_currency as amount_currency,
                account_move_line.partner_id AS partner_id,
                partner.name AS partner_name,
                COALESCE(trust_property.value_text, 'normal') AS partner_trust,
                COALESCE(account_move_line.currency_id, journal.currency_id) AS report_currency_id,
                account_move_line.payment_id AS payment_id,
                COALESCE(account_move_line.date_maturity, account_move_line.date) AS report_date,
                account_move_line.expected_pay_date AS expected_pay_date,
                move.move_type AS move_type,
                move.name AS move_name,
                move.ref AS move_ref,
                move.invoice_date as invoice_date,
                account.code || ' ' || account.name AS account_name,
                account.code AS account_code,""" + ','.join([("""
                CASE WHEN period_table.period_index = {i}
                THEN %(sign)s * ROUND((
                    account_move_line.balance - COALESCE(SUM(part_debit.amount), 0) + COALESCE(SUM(part_credit.amount), 0)
                ) * currency_table.rate, currency_table.precision)
                ELSE 0 END AS period{i}""").format(i=i) for i in range(6)]) + """
            FROM account_move_line
            JOIN account_move move ON account_move_line.move_id = move.id
            JOIN account_journal journal ON journal.id = account_move_line.journal_id
            JOIN account_account account ON account.id = account_move_line.account_id
            JOIN res_partner partner ON partner.id = account_move_line.partner_id
            LEFT JOIN ir_property trust_property ON (
                trust_property.res_id = 'res.partner,'|| account_move_line.partner_id
                AND trust_property.name = 'trust'
                AND trust_property.company_id = account_move_line.company_id
            )
            JOIN {currency_table} ON currency_table.company_id = account_move_line.company_id
            LEFT JOIN LATERAL (
                SELECT part.amount, part.debit_move_id
                FROM account_partial_reconcile part
                WHERE part.max_date <= %(date)s
            ) part_debit ON part_debit.debit_move_id = account_move_line.id
            LEFT JOIN LATERAL (
                SELECT part.amount, part.credit_move_id
                FROM account_partial_reconcile part
                WHERE part.max_date <= %(date)s
            ) part_credit ON part_credit.credit_move_id = account_move_line.id
            JOIN {period_table} ON (
                period_table.date_start IS NULL
                OR COALESCE(account_move_line.date_maturity, account_move_line.date) <= DATE(period_table.date_start)
            )
            AND (
                period_table.date_stop IS NULL
                OR COALESCE(account_move_line.date_maturity, account_move_line.date) >= DATE(period_table.date_stop)
            )
            WHERE account.internal_type = %(account_type)s 
            AND account.exclude_from_aged_reports IS NOT TRUE
            {supplier_type}
            GROUP BY account_move_line.id, partner.id, trust_property.id, journal.id, move.id, account.id,
                period_table.period_index, currency_table.rate, currency_table.precision
            HAVING ROUND(account_move_line.balance - COALESCE(SUM(part_debit.amount), 0) + COALESCE(SUM(part_credit.amount), 0), currency_table.precision) != 0
        """).format(
                move_line_fields=self._get_move_line_fields('account_move_line'),
                currency_table=self.env['res.currency']._get_query_currency_table(options),
                period_table=self._get_query_period_table(options),
                supplier_type='AND partner.is_company in (TRUE, FALSE)' \
                    if not supplier_type_dict.get(True) else supplier_type_dict.get(True),
                )
        
        params = {
            'account_type': options['filter_account_type'],
            'sign': 1 if options['filter_account_type'] == 'receivable' else -1,
            'date': options['date']['date_to'],
        }
        
        return self.env.cr.mogrify(query, params).decode(self.env.cr.connection.encoding)

    # def _get_table(self, options):
    #     headers, lines = super(ReportAccountAgedPartner, self)._get_table(options)
    #     if self._name == 'account.aged.payable':
    #         for line in lines:
    #             if line.get('unfoldable') is False:
    #                 print("### line _get_table ### \n", line, "\n ### ###")
    #     return headers, lines