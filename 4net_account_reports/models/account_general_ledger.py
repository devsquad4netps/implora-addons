from odoo import models, fields, api, _
from odoo.tools.misc import format_date, DEFAULT_SERVER_DATE_FORMAT
from datetime import timedelta


class AccountGeneralLedgerReport(models.AbstractModel):
    _inherit = "account.general.ledger"

    @api.model
    def _get_columns_name(self, options):
        columns_names = [
            {'name': 'Account', 'style': 'width: 15%; border: 1px solid #ddd;'},
            {'name': 'Nama Account', 'style': 'width: 10%'},
            {'name': _('Date'), 'class': 'date'},
            {'name': _('Communication')},
            {'name': _('Partner')},
            {'name': _('Debit'), 'class': 'number'},
            {'name': _('Credit'), 'class': 'number'},
            {'name': _('Balance'), 'class': 'number'}
        ]
        if self.user_has_groups('base.group_multi_currency'):
            columns_names.insert(5, {'name': _('Currency'), 'class': 'number'})
        return columns_names
    
    ####################################################
    # COLUMN/LINE HELPERS
    ####################################################

    @api.model
    def _get_account_title_line(self, options, account, amount_currency, debit, credit, balance, has_lines):
        has_foreign_currency = account.currency_id and account.currency_id != account.company_id.currency_id or False
        unfold_all = self._context.get('print_mode') and not options.get('unfolded_lines')

        # name = '%s %s' % (account.code, account.name)
        name = f'{account.code}'
        columns = [
            {'name': '', 'class': 'text-left'},
            {'name': '', 'class': 'text-left'},
            {'name': self.format_value(debit), 'class': 'number'},
            {'name': self.format_value(credit), 'class': 'number'},
            {'name': self.format_value(balance), 'class': 'number'},
        ]
        if self.user_has_groups('base.group_multi_currency'):
            columns.insert(0, {'name': has_foreign_currency and self.format_value(amount_currency, currency=account.currency_id, blank_if_zero=True) or '', 'class': 'number'})
            columns.insert(1, {'name': '', 'class': 'text-left'})
        columns.insert(0, {'name': str(account.name), 'class': 'text-left', 'style': 'width:10%'},)
        return {
            'id': 'account_%d' % account.id,
            'name': name,
            'columns': columns,
            'level': 1,
            'unfoldable': has_lines,
            'unfolded': has_lines and 'account_%d' % account.id in options.get('unfolded_lines') or unfold_all,
            'colspan': 1,
            'class': 'o_account_reports_totals_below_sections' if self.env.company.totals_below_sections else '',
        }
    
    @api.model
    def _get_initial_balance_line(self, options, account, amount_currency, debit, credit, balance):
        columns = [
            {'name': self.format_value(debit), 'class': 'number'},
            {'name': self.format_value(credit), 'class': 'number'},
            {'name': self.format_value(balance), 'class': 'number'},
        ]

        has_foreign_currency = account.currency_id and account.currency_id != account.company_id.currency_id or False
        if self.user_has_groups('base.group_multi_currency'):
            columns.insert(0, {'name': has_foreign_currency and self.format_value(amount_currency, currency=account.currency_id, blank_if_zero=True) or '', 'class': 'number'})
            columns.insert(0, {'name': '', 'class': 'text-left',})
        return {
            'id': 'initial_%d' % account.id,
            'class': 'o_account_reports_initial_balance',
            'name': _('Initial Balance'),
            'parent_id': 'account_%d' % account.id,
            'columns': columns,
            'colspan': 4,
        }
    
    @api.model
    def _get_aml_line(self, options, account, aml, cumulated_balance):
        payment_id = False
        aml_obj = self.env['account.move.line']
        partner_name = str(aml['partner_name'] or '')
        if aml['payment_id']:
            caret_type = 'account.payment'
            # * get invoice/bill partner
            payment_obj = self.env[caret_type]
            payment_id = payment_obj.browse(aml['payment_id'])
            reconciled_bill_ids = payment_id.reconciled_bill_ids
            if partner_name and partner_name != '':
                for bill in reconciled_bill_ids:
                    if bill.partner_id.name and str(bill.partner_id.name) != str(aml['partner_name']):
                        partner_name += ', ' + str(bill.partner_id.name) + ' '
                reconciled_invoice_ids = payment_id.reconciled_invoice_ids
                for invoice in reconciled_invoice_ids:
                    if invoice.partner_id.name and str(invoice.partner_id.name) != str(aml['partner_name']):
                        partner_name += ', ' + str(invoice.partner_id.name) + ' '
        else:
            caret_type = 'account.move'
            # * get invoice/bill partner
            move_id = aml_obj.browse(aml['id']).move_id
            if move_id.partner_id.name and move_id.partner_id.name != partner_name:
                partner_name += ', ' + str(move_id.partner_id.name) + ' '

        if (aml['currency_id'] and aml['currency_id'] != account.company_id.currency_id.id) or account.currency_id:
            currency = self.env['res.currency'].browse(aml['currency_id'])
        else:
            currency = False
        
        other_ref = ''
        aml_bank = False
        aml_id = aml_obj.browse(aml.get('id'))
        if aml.get('payment_id'):
            payment_obj = self.env['account.payment']
            payment_id = payment_obj.browse(aml.get('payment_id'))
            if payment_id.is_misc_payment:
                if aml.get('account_id') == payment_id.journal_id.default_account_id.id:
                    other_ref = payment_id.ref
                    aml_bank = True
                else:
                    other_ref = aml['ref'] if not aml_id.payment_misc_id else aml_id.payment_misc_id.name
        comm_desc = other_ref if aml_id.payment_misc_id or aml_bank \
            else self._format_aml_name(aml['name'], aml['ref'])

        columns = [
            {'name': '', 'class': 'text-left',},
            {'name': format_date(self.env, aml['date']), 'class': 'date'},
            # {'name': self._format_aml_name(aml['name'], aml['ref']), 'class': 'o_account_report_line_ellipsis'},
            {'name': comm_desc, 'class': 'o_account_report_line_ellipsis'},
            # ! {'name': aml['partner_name'], 'class': 'o_account_report_line_ellipsis'},
            {'name': partner_name, 'class': 'o_account_report_line_ellipsis'},
            {'name': self.format_value(aml['debit'], blank_if_zero=True), 'class': 'number'},
            {'name': self.format_value(aml['credit'], blank_if_zero=True), 'class': 'number'},
            {'name': self.format_value(cumulated_balance), 'class': 'number'},
        ]
        if self.user_has_groups('base.group_multi_currency'):
            columns.insert(4, {'name': currency and aml['amount_currency'] and self.format_value(aml['amount_currency'], currency=currency, blank_if_zero=True) or '', 'class': 'number'})
        return {
            'id': aml['id'],
            'caret_options': caret_type,
            'parent_id': 'account_%d' % aml['account_id'],
            'name': aml['move_name'],
            'columns': columns,
            'level': 2,
        }
        
    @api.model
    def _get_total_line(self, options, debit, credit, balance):
        return {
            'id': 'general_ledger_total_%s' % self.env.company.id,
            'name': _('Total'),
            'class': 'total',
            'level': 1,
            'columns': [
                {'name': self.format_value(debit), 'class': 'number'},
                {'name': self.format_value(credit), 'class': 'number'},
                {'name': self.format_value(balance), 'class': 'number'},
            ],
            'colspan': self.user_has_groups('base.group_multi_currency') and 6 or 5,
        }