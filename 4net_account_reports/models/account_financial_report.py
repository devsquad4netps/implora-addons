import ast
import json

from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.tools import float_is_zero, ustr
from odoo.exceptions import ValidationError
from odoo.osv import expression


class ReportAccountFinancialReport(models.Model):
    _inherit = "account.financial.html.report"
    
    report_hierarchy = fields.Boolean(
        "Report Hierarchy"
    )

class AccountFinancialReportLine(models.Model):
    _inherit = "account.financial.html.report.line"

    report_hierarchy = fields.Boolean(
        "Report Hierarchy"
    )
    account_view_custom_id = fields.Many2one(
        "account.account", "Account View"
    )
    account_custom_id = fields.Many2one(
        "account.account", "Account"
    )
    is_parent = fields.Boolean(
        "Account Parent ?"
    )

    @api.onchange('account_view_custom_id')
    def _onchange_account_view_custom_id(self):
        if self.account_view_custom_id:
            self.name = f'{self.account_view_custom_id.display_name}'
            self.code = f'B{self.account_view_custom_id.name[:3]}'
    
    @api.onchange('account_custom_id','is_parent')
    def _onchange_account_custom_id(self):
        account_obj = self.env['account.account']
        if self.account_custom_id and self.is_parent:
            account_src = account_obj.search([('parent_id','=',self.account_custom_id.id)])
            formulas = ''
            for ac in account_src:
                if not formulas:
                    formulas = f'B{ac.name[:3]}'
                else:
                    formulas = f'{formulas} + B{ac.name[:3]}'
            self.formulas = formulas

        if self.account_custom_id and not self.is_parent:
            account_src = account_obj.search([('parent_id','=',self.account_custom_id.id)]).mapped("code")
            self.domain = [('account_id.code', 'in', tuple(account_src))]

