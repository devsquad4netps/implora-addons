import io
import base64
import markupsafe
from dateutil import parser
from odoo.tools.misc import xlsxwriter
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from odoo.addons.account_reports.models.formula import FormulaSolver

class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    def get_xlsx_pnl_hierarchy(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style_header = workbook.add_format({'font_name': 'Arial', 'bold': True })
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'border': True, 'bottom': 1,'top' : 1})
        title_style.set_align('center')
        title_style.set_align('vcenter')
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1,'top' : 1, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'border': True, 'bottom': 1,'top' : 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'border': True, 'bottom': 1,'top' : 1, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'border': True, 'bottom': 1,'top' : 1})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'border': True, 'bottom': 1,'top' : 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'border': True, 'bottom': 1,'top' : 1})

        #Set the first column width to 50
        sheet.set_column(0, 0, 50)

        y_offset = 5
        headers, lines = self.with_context(no_format=True, print_mode=True, prefetch_fields=False)._get_table(options)

        # header
        date_from = parser.parse(options['date']['date_from']).strftime("%d %b %Y")
        date_to = parser.parse(options['date']['date_to']).strftime("%d %b %Y")
        date_year = parser.parse(options['date']['date_from']).strftime("%Y")
        year_start = f'{date_year}'
        for x in options['comparison']['periods']:
            year_start = f'{year_start}, {x["string"]}' 

        if not options['comparison']['periods']:
            period = f'Periode {date_from} - {date_to}'
        else:
            period = f'Periode {year_start}'

        sheet.write(0, 0, self.env.company.name, title_style_header)
        sheet.write(1, 0, sheet.name, title_style_header)
        sheet.write(2, 0, period, title_style_header)
        # image
        image_company = io.BytesIO(base64.b64decode(self.env.company.logo))
        sheet.insert_image('H1', 'image.png', {'image_data': image_company, 'x_scale': 0.5, 'y_scale': 0.4})

        # Add headers.
        for header in headers:
            x_offset = 0
            for column in header:
                column_name_formated = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                colspan = column.get('colspan', 1)
                if colspan == 1:
                    sheet.write(y_offset, x_offset, column_name_formated, title_style)
                else:
                    sheet.merge_range(y_offset, x_offset, y_offset, x_offset + colspan - 1, column_name_formated, title_style)
                x_offset += colspan
            y_offset += 1

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        # Add lines.
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)

            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()

        return generated_file