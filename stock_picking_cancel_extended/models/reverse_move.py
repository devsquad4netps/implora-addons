# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def _prepare_default_reversal(self, move):
        reverse_date = move.date
        return {
            'ref': _('Reversal of: %s', move.name),
            'date': reverse_date,
            'invoice_date': move.is_invoice(include_receipts=True) and (move.date) or False,
            'journal_id': move.journal_id.id,
            'invoice_payment_term_id': None,
            'invoice_user_id': move.invoice_user_id.id,
            'auto_post': True if reverse_date > fields.Date.context_today(self) else False,
        }

    def _reverse_done_moves(self, moves):
        self.ensure_one()
        default_values_list = []
        for move in moves:
            default_values_list.append(self._prepare_default_reversal(move))
        batches = [
            [self.env['account.move'], [], True],   # Moves to be cancelled by the reverses.
            [self.env['account.move'], [], False],  # Others.
        ]
        for move, default_vals in zip(moves, default_values_list):
            is_auto_post = bool(default_vals.get('auto_post'))
            is_cancel_needed = not is_auto_post
            batch_index = 0 if is_cancel_needed else 1
            batches[batch_index][0] |= move
            batches[batch_index][1].append(default_vals)
        # Handle reverse method.
        moves_to_redirect = self.env['account.move']
        for moves, default_values_list, is_cancel_needed in batches:
            new_moves = moves._reverse_moves(default_values_list, cancel=is_cancel_needed)
            print("### reversal moves from canceled stock picking ###", new_moves)
            # if self.refund_method == 'modify':
            #     moves_vals_list = []
            #     for move in moves.with_context(include_business_fields=True):
            #         moves_vals_list.append(move.copy_data({'date': self.date if self.date_mode == 'custom' else move.date})[0])
            #     new_moves = self.env['account.move'].create(moves_vals_list)
            moves_to_redirect |= new_moves
        