import json
from odoo import models,api, _
from odoo.exceptions import UserError, ValidationError

class PayOrdereReport(models.AbstractModel):
    _name = "report.4net_reports.report_payment_receipt_purchase"
 
    @api.model
    def _get_report_values(self, docids, data=None):
        name = '4net_reports.report_payment_receipt_purchase'
        report = self.env['ir.actions.report']._get_report_from_name(name)
        docs = self.env[report.model].browse(docids)
        partner = docs.mapped("partner_id")
        if len(partner) > 1:
            raise ValidationError(_("Vender Tidak Boleh Beda"))
        memo = ''
        result_line = []
        amount_total = 0
        no = 1
        surat_jalan = po_name = ''
        invoice = False
        for po in docs:
            picking = self.env['stock.picking'].search([('purchase_id','=',po.id),('state','=','done')], limit=1)
            po_name = po.name
            if picking:
                surat_jalan = picking.receipt_no
            for inv in po.invoice_ids.filtered(lambda l: l.amount_residual > 0 and l.state == 'posted'):
                invoice = inv
                json_inv = json.loads(inv.sudo().tax_totals_json)
                amount = json_inv['amount_total']
                result_line.append({
                    'no'            : no,
                    'currency_id'   : inv.currency_id,
                    'no_fak'        : inv.ref,
                    'tgl'           : inv.invoice_date,
                    'jatuh_tempo'   : inv.invoice_date_due,
                    'surat_jalan'   : surat_jalan,
                    'no_po'         : po_name,
                    'nominal'       : amount,
                })
                no += 1
                amount_total += amount
                str_narration = str(inv.narration)
                if str_narration != '<p><br></p>':
                    if memo and inv.narration:
                        memo = inv.narration + '\n' + memo + '\n'
                    else:
                        if inv.narration:
                            memo = inv.narration + '\n'
        if not result_line:
            raise ValidationError(_("Data kosong atau status invoice masih draft / sudah di bayar"))
        nilai_akhir = 7 - len(result_line)
        if nilai_akhir > 0:
            for x in range(0, nilai_akhir):
                result_line.append({
                    'no'            : no,
                    'currency_id'   : ' ',
                    'no_fak'        : ' ',
                    'tgl'           : ' ',
                    'jatuh_tempo'   : ' ',
                    'surat_jalan'   : ' ',
                    'no_po'         : ' ',
                    'nominal'       : 'no',
                })
                no += 1
        return {
            'docs': invoice,
            'lines': result_line,
            'amount_total' : amount_total,
            'memo' : memo,
            'doc_model': report.model,
            'report_type': data.get('report_type') if data else '',
        }