
import json
from terbilang import Terbilang
from odoo import models, api, fields, _

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    sequence = fields.Integer()

class ReportReceipt(models.AbstractModel):
    _name = 'report.4net_reports.report_recieve_items'
    _description = 'Receipt Print Out'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['stock.picking'].browse(docids)

        len_lines = 0
        line_nums = []
        picking_id = False
        move_line_ids = []
        for doc in docs:
            len_lines = len(doc.move_line_ids_without_package)
            line_no = 1
            if len_lines >= 1:
                picking_id = doc
                for line in doc.move_line_ids_without_package:
                    line.sequence = line_no
                    move_line_ids.append(line.id)
                    line_no += 1

                max_num = len_lines + 1
                if len_lines >= 5:
                    for x in range(5, max_num):
                        if divmod(x, 5)[1] == 0:
                            line_nums.append(x)
                        elif divmod(x, 5)[1] != 0 and x == len_lines:
                            # print("### x max_num ###", x, max_num)
                            if x <= max_num:
                                line_nums.append(x)
                else:
                    line_nums += [len_lines] if len_lines  > 1 else [len_lines + 1]

        # print("### len_lines ###", len_lines)
        print("### line_nums ###", line_nums)
        
        items = {}
        if line_nums and picking_id and move_line_ids:
            aml_obj = self.env['stock.move.line']
            page = 1
            start = 1
            for num in line_nums:
                domain = ['&', '&',
                    ('id', 'in', move_line_ids),
                    ('picking_id', '=', picking_id.id),
                    ('sequence', '>=', start),
                    ('sequence', '<=', num),
                ]
                aml_ids = aml_obj.search(domain)
                items[page] = aml_ids
                page += 1
                start = num + 1

        # print("### items ###", items)
        pages = [x for x in items.keys()]
        # print("### pages ###", pages)

        return {
            'doc_model': 'account.move',
            'doc_ids': docids,
            'docs': docs,
            'items': items,
            'pages': pages,
        }
        