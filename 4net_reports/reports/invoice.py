# -*- coding: utf-8 -*-

import json
from terbilang import Terbilang
t = Terbilang(sep='.')
from odoo import models, api, fields, _

class AccountMove(models.Model):
    _inherit = 'account.move'

    amount_total_says = fields.Char(compute='_get_amount_total_says')
    
    def _get_amount_total_says(self):
        for record in self:
            amount_total = 0
            if record.move_type == 'out_invoice':
                tax_totals_dict = json.loads(record.tax_totals_json)
                # print("### totals_tax ###", tax_totals_dict)
                amount_total = round(tax_totals_dict.get('amount_total'), 2)
                # print("### amount_total ###", amount_total)
            record.amount_total_says = str(t.parse(amount_total).getresult()).title() + ' Rupiah'
    
class ReportInvoice(models.AbstractModel):
    _name = 'report.4net_reports.report_invoice'
    _description = 'Invoice Print Out'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids)

        len_lines = 0
        line_nums = []
        move_id = False
        invoice_line_ids = []
        for doc in docs:
            refund_amount = 0
            # if json.loads(doc.invoice_payments_widget):
            #     invoice_payments_contents = json.loads(doc.invoice_payments_widget).get('content')
            #     # print("### invoice_payments_contents ###", invoice_payments_contents)
            #     for content in invoice_payments_contents:
            #         if self.env['account.move'].browse(content.get('move_id')).move_type == 'out_refund':
            #             refund_amount += content.get('amount') or 0
            # print("### refund_amount ###", refund_amount)

            len_lines = len(doc.invoice_line_ids)
            line_no = 1
            if len_lines >= 1:
                move_id = doc
                for line in doc.invoice_line_ids:
                    line.sequence = line_no
                    invoice_line_ids.append(line.id)
                    # if len_lines < 5:
                    #     line_nums.append(line_no)
                    line_no += 1
                max_num = len_lines + 1
                if len_lines >= 5:
                    for x in range(5, max_num):
                        if divmod(x, 5)[1] == 0:
                            line_nums.append(x)
                        elif divmod(x, 5)[1] != 0 and x == len_lines:
                            if x <= max_num:
                                line_nums.append(x)
                else:
                    line_nums += [len_lines] if len_lines  > 1 else [len_lines + 1]

        # print("### line_nums ###", line_nums)
        
        items = {}
        if line_nums and move_id and invoice_line_ids:
            aml_obj = self.env['account.move.line']
            page = 1
            start = 1
            for num in line_nums:
                domain = ['&', '&',
                    ('id', 'in', invoice_line_ids),
                    ('move_id', '=', move_id.id),
                    ('sequence', '>=', start),
                    ('sequence', '<=', num),
                ]
                aml_ids = aml_obj.search(domain)
                items[page] = aml_ids
                page += 1
                start = num + 1
        
        long_desc_dict = {}
        pages = [x for x in items.keys()]
        for page in pages:
            long_desc_count = 0
            for x in items[page]:
                if len(str(x.product_id.description_sale or '')) >= 29:
                    long_desc_count += 1
            long_desc_dict[page] = long_desc_count
            if long_desc_dict[page] > 0 and page == max(pages) and len(items[page]) >= 3:
                new_item = []
                for x in range(long_desc_count):
                    last_items = items[page][-1]
                    new_item = [x for x in items[page] if x.id != last_items.id]
                items[page] = new_item
                # print("### new_item ###", new_item)
                new_page = max(pages) + 1
                pages.append(new_page)
                items[new_page] = last_items
            # print("### items per page ###", page, items[page])
        
        return {
            'doc_model': 'account.move',
            'doc_ids': docids,
            'docs': docs,
            'items': items,
            'pages': pages,
            'refund_amount': refund_amount,
        }
