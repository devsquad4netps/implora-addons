import json
from odoo import models,api, _
from odoo.exceptions import UserError, ValidationError
import textwrap

class Report4net_reportsPurchaseOrder(models.AbstractModel):
    _name = "report.4net_reports.report_purchase_order"

    @api.model
    def _get_report_values(self, docids, data=None):
        name = '4net_reports.report_purchase_order'
        report = self.env['ir.actions.report']._get_report_from_name(name)
        docs = self.env[report.model].browse(docids)
        number_end = 0
        list_line = []
        date_planned = ''
        for po in docs:
            for line in po.order_line:
                date_planned = line.date_planned
                number_end += 1
                name_item = line.product_id.name
                description_purchase = line.product_id.description_purchase
                # karakter_name = po.get_karakter(description_purchase)
                # kata_name = po.get_kata(description_purchase)
                line_item = name_item
                if name_item:
                    line_item = textwrap.wrap(name_item, 12)
                line_description = []
                if description_purchase:
                    line_description = textwrap.wrap(description_purchase, 21)
                    if len(line_item) > len(line_description):
                        jumlah = len(line_item) - len(line_description)
                        for p in range(0, jumlah):
                            line_description.append('')
                    if len(line_description) > len(line_item):
                        jumlah = len(line_description) - len(line_item)
                        for p in range(0, jumlah):
                            line_item.append('')
                max_data = 0
                if line_item and line_description:
                    max_data = max(len(line_item),len(line_description))
                num = 0
                for i in range(0, max_data):
                    if num == 0:
                        list_line.append({
                            'currency_id'   : po.currency_id,
                            'product'       : line_item[num],
                            # 'description'   : line_description[num],
                            'description'   : description_purchase or '',
                            'product_qty'   : line.product_qty,
                            'product_uom'   : f' {line.product_uom.name}',
                            'price_unit'    : line.price_unit,
                            'price_tax'     : line.price_tax,
                            'date_planned'  : line.date_planned,
                            'price_subtotal': line.price_subtotal,
                        })
                    else:
                        list_line.append({
                            'currency_id'   : po.currency_id,
                            'product'       : line_item[num],
                            # 'description'   : line_description[num],
                            'description'   : '',
                            'product_qty'   : '',
                            'product_uom'   : '',
                            'price_unit'    : '',
                            'price_tax'     : '',
                            'date_planned'  : '',
                            'price_subtotal': '',
                        })
                    num += 1
        return {
                'docs': docs,
                'date_planned' : date_planned,
                'lines': list_line,
                'doc_model': report.model,
                'report_type': data.get('report_type') if data else '',
            }


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    def get_karakter(self, data):
        return len(data)
    
    def get_kata(self, data):
        kata = data.split(' ')
        return len(kata)