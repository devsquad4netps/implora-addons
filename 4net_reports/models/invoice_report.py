from odoo import api, fields, models

class invoice_report(models.Model):
    _inherit="account.move"

    def print_invoice(self):
        return self.env.ref('4net_reports.act_print_invoice').report_action(self)