from odoo import api, fields, models, _
from odoo.exceptions import AccessError, UserError, ValidationError

class asset(models.Model):
    _inherit = "account.asset"

    def action_print(self):
        return self.env.ref('4net_reports.act_print_report_recieve_item').report_action(self)