from odoo import api, fields, models

class purchase_order(models.Model):
    _inherit="purchase.order"

    receipt_no = fields.Char(string="Receipt No")
    payment_state = fields.Selection([
        ('not_paid', 'Not Paid'),
        ('in_payment', 'In Payment'),
        ('paid', 'Paid'),
        ('partial', 'Partially Paid'),
        ('reversed', 'Reversed'),
        ('invoicing_legacy', 'Invoicing App Legacy'),], string="Payment Status", store=True
    )

    @api.onchange('partner_id')
    def _onchange_partner_implora(self):
        for rec in self:
            if rec.partner_id:
                rec.partner_ref = rec.partner_id.ref
    
    check_payment_state = fields.Boolean(compute="_payment_state")

    # @api.depends("invoice_ids")
    def _payment_state(self):
        for o in self:
            # print("### invoice_ids ###", o.name, o.invoice_ids)
            if o.invoice_ids:
                not_paid = o.invoice_ids.filtered(lambda l:l.payment_state == "not_paid")
                in_payment = o.invoice_ids.filtered(lambda l:l.payment_state == "in_payment")
                paid = o.invoice_ids.filtered(lambda l:l.payment_state == "paid")
                partial = o.invoice_ids.filtered(lambda l:l.payment_state == "partial")
                reversed = o.invoice_ids.filtered(lambda l:l.payment_state == "reversed")
                invoicing_legacy = o.invoice_ids.filtered(lambda l:l.payment_state == "invoicing_legacy")
                o.payment_state = ""
                if not_paid: 
                    o.payment_state = "not_paid"
                if in_payment: 
                    o.payment_state = "in_payment"
                if paid: 
                    o.payment_state = "paid"
                if partial: 
                    o.payment_state = "partial"
                if reversed: 
                    o.payment_state = "reversed"
                if invoicing_legacy: 
                    o.payment_state = "invoicing_legacy"
            else: 
                o.payment_state = ""
            o.check_payment_state = True if o.invoice_ids else False

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    def _get_product_purchase_description(self, product_lang):
        self.ensure_one()
        name = product_lang.description_purchase
        if not product_lang.description_purchase:
            name = product_lang.product_tmpl_id.description_purchase
        return name
