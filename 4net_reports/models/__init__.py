# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import invoice_report
from . import purchase_order
from . import stock_picking
from . import asset