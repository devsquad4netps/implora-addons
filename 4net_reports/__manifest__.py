# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Custom Report',
    'version': '1.0',
    'category': 'Report/Purcashing/Accounting',
    'sequence': 36,
    'summary': 'Purchase Order, Payment Receipt, Invoice Report',
    'description': "",
    'website': '',
    'depends': ['account','purchase','web','account_consolidation','report_qweb_element_page_visibility'],
    'data': [
        'reports/paperformat.xml',
        'reports/report_layout.xml',
        'reports/report_purchase_order.xml',
        'reports/pay_order_inv.xml',
        'reports/pay_order.xml',
        'reports/trial_balance.xml',
        'reports/invoice_items.xml',
        'reports/invoice_view.xml',
        'reports/report_invoice.xml',
        'reports/receipt_view.xml',
        'reports/recieve_item.xml',
        'views/report_invoice.xml',
        'views/inherit_purchase_order.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
    'external_dependencies': {'python': ['terbilang']}
}
