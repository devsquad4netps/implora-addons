from datetime import timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools.misc import get_lang
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare, float_round

ks_name = "Multi Discount"

class SaleOrder(models.Model):
    _inherit = "sale.order"

    amount_discount_line =fields.Float(
        'Discount Line', compute="_compute_multi_discount", digits='Product Price'
    )
    amount_discount_global = fields.Float(
        'Discount Global', compute="_compute_multi_discount", digits='Product Price'
    )
    total_discount_amount = fields.Float(
        'Total Discount', compute="_compute_multi_discount", digits='Product Price'
    )

    # @api.depends("rounding_discount_amount","discount_rate","discount_type","invoice_line_ids","invoice_line_ids.multi_discount","invoice_line_ids.tax_ids")
    @api.depends("order_line.multi_discount")
    def _compute_multi_discount(self):
        for rec in self:
            # rec.invoice_line_ids._onchange_price_subtotal()
            # amount_untaxed = sum(rec.invoice_line_ids.mapped("price_subtotal_inv"))
            # rec.amount_untaxed = amount_untaxed
            rec.amount_discount_line = sum(rec.order_line.mapped("amount_discount"))
            rec.amount_discount_global = 0 #rec.update_discount_global(amount_untaxed)
            # currencies = rec._get_lines_onchange_currency().currency_id
            # currency = currencies if len(currencies) == 1 else rec.company_id.currency_id
            total_discount = rec.amount_discount_line + rec.amount_discount_global
            # tax = rec._compute_tax_global()
            # amount_tax = 0
            # account_tax_inv_id = False
            # if not tax:
            #     rec.amount_total_inv = amount_untaxed - rec.amount_discount_global
            # else:
            #     if tax.type_invoice == 'include':
            #         amount_tax, account_tax_inv_id = rec.tax_include(tax, amount_untaxed)
            #         total_discount = total_discount / 1.11
            #     else:
            #         amount_tax, account_tax_inv_id = rec.tax_exclude(tax, amount_untaxed)
            # rec.amount_tax_inv = amount_tax
            # rec.account_tax_inv_id = account_tax_inv_id
            disocunt = float_round(total_discount, precision_digits=4, rounding_method='DOWN')
            rec.total_discount_amount = round(disocunt, currency.decimal_places)

    def _create_invoices(self, grouped=False, final=False, date=None):
        res_move = super(SaleOrder, self)._create_invoices(grouped=grouped, final=final, date=date)
        for move in res_move:
            amount = move.total_discount_amount
            in_draft_mode = self != self._origin
            new_tax_line = self.env['account.move.line']
            create_method = in_draft_mode and new_tax_line.new or new_tax_line.create
            already_exists = move.line_ids.filtered(lambda line: line.account_id.id == move.account_discount_id.id)
            dict = {
                'move_name': self.name,
                'name': ks_name,
                'price_unit': amount,
                'quantity': 1,
                'debit':  amount if amount > 0.0 else 0.0,
                'credit': amount if amount < 0.0 else 0.0,
                'account_id': move.account_discount_id.id,
                'amount_currency': amount if amount > 0.0 else -amount,
                'move_id': move.id,
                'date': move.date,
                'exclude_from_invoice_tab': True,
                'partner_id': move.partner_id.id,
                'company_id': move.company_id.id,
                'company_currency_id': move.company_currency_id.id,
            }
            if not already_exists and amount != 0:
                print(move.line_ids,'lalalalaal')
                move.line_ids += create_method(dict)
            move.calculate_discount_multi()
        return res_move
        
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    multi_discount = fields.Char(
        'Discounts'
    )
    amount_discount = fields.Float(
        'Discount Amount', default=0
    )

    def _get_subtotal_inv(self):
        for line in self:
            if line.multi_discount:
                price_subtotal = line.price_subtotal
                splited_discounts = line.multi_discount.split("+")
                for discount in splited_discounts:
                    if discount != '':
                        if ',' in discount:
                            discount = discount.replace(",",".")
                        if price_subtotal != 0 and float(discount) != 0:
                            price_subtotal -= price_subtotal * (float(discount) / 100.0)
                line.price_subtotal = price_subtotal
                line.amount_discount = (line.product_uom_qty * line.price_unit) - line.price_subtotal
            else:
                line.multi_discount = 0 
    
    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id','multi_discount')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            line.update({
                # 'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': price * line.product_uom_qty,
                'price_subtotal': price * line.product_uom_qty,
            })
            line._get_subtotal_inv()
            if self.env.context.get('import_file', False) and not self.env.user.user_has_groups('account.group_account_manager'):
                line.tax_id.invalidate_cache(['invoice_repartition_line_ids'], [line.tax_id.id])
    
    def _prepare_invoice_line(self, **optional_values):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        :param optional_values: any parameter that should be added to the returned invoice line
        """
        self.ensure_one()
        res = {
            'display_type': self.display_type,
            'sequence': self.sequence,
            'name': self.name,
            'product_id': self.product_id.id,
            'product_uom_id': self.product_uom.id,
            'quantity': self.qty_to_invoice,
            'discount': self.discount,
            'multi_discount' : self.multi_discount,
            # 'amount_discount' : self.amount_discount,
            'price_unit': self.price_unit,
            'price_unit_inv' : self.price_unit,
            'tax_ids': [(6, 0, self.tax_id.ids)],
            'analytic_account_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'sale_line_ids': [(4, self.id)],
        }
        if optional_values:
            res.update(optional_values)
        if self.display_type:
            res['account_id'] = False
        return res