# -*- coding: utf-8 -*-
{
    'name'          : 'Custom Sales',
    'author'        : 'Tian',
    'category'      : 'Sales',
    'summary'       : 'Custom Sales',
    'description'   : """Custom Sales""",
    'website'       : '',
    'version'       : '1.0',
    'sequence'      : 1,
    'depends'       : ['base','4net_multi_discounts','sale'],
    'data':          [
                        # 'security/ir.model.access.csv',
                        'views/sale_order_view.xml',
                        # 'views/account_account_view.xml',
                        # 'views/res_config_view.xml',
                        # 'views/account_invoice_view.xml',
                      ],
    # 'assets'        : {
    #                     'web.assets_qweb': [
    #                         '4net_multi_discounts/static/src/xml/tax_totals.xml',
    #                     ],
    #                  },
    'installable': True,
}